﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace LLVK;


/// <summary>
/// Contains an array of null-terminated, UTF-8 encoded C-string pointers that are pre-pinned in memory
/// (<c>const char* const*</c>).
/// </summary>
/// <remarks>
/// Uses natively allocated memory, be sure to call <see cref="Dispose"/>. Not thread-safe. Should never be modified
/// while in use by native code.
/// </remarks>
public sealed unsafe class NativeStringListUtf8 : IDisposable, IEnumerable<string>
{
	#region Fields
	/// <summary>The number of C-strings in the array of pointers.</summary>
	public uint Count { get; private set; }

	/// <summary>The native data for the C-string at the given index. Throws on bad index or if disposed.</summary>
	/// <param name="index">The index of the string to access.</param>
	public ReadOnlySpan<byte> this [uint index] => IsDisposed
		? throw new ObjectDisposedException(nameof(NativeStringListUtf8))
		: (index >= Count)
			? throw new IndexOutOfRangeException()
			: new(_data[index], (int)_lengths[index]);

	/// <summary>The raw array of c-string pointers. May be <c>null</c> if no strings are in the list.</summary>
	public byte** Data => IsDisposed ? throw new ObjectDisposedException(nameof(NativeStringListUtf8)) : _data;

	// Natively allocated pointer array, containing the c-string pointers
	private byte** _data;

	// The saved lengths of the individual strings (length provides the capacity)
	private uint[] _lengths = Array.Empty<uint>();

	/// <summary>If the string list has been disposed.</summary>
	public bool IsDisposed { get; private set; }
	#endregion // Fields


	/// <summary>Creates an empty list with the given capacity.</summary>
	/// <param name="capacity">The initial capacity of the list.</param>
	public NativeStringListUtf8(uint capacity = 0) => EnsureCapacity(capacity);

	/// <summary>Creates a list with the given strings as the initial contents.</summary>
	/// <param name="strings">The strings to initialize the list with.</param>
	public NativeStringListUtf8(IEnumerable<string> strings)
		: this(strings.TryGetNonEnumeratedCount(out var count) ? (uint)count : 0)
	{
		foreach (var str in strings) {
			Add(str);
		}
	}

	~NativeStringListUtf8() => Dispose();

	public void Dispose()
	{
		if (!IsDisposed) {
			if (_data != null) {
				Clear();
				NativeMemory.Free(_data);
				_data = null;
			}
			_lengths = Array.Empty<uint>();
		}
		IsDisposed = true;
		GC.SuppressFinalize(this);
	}


	/// <summary>Gets the native string at the given index as a managed string.</summary>
	/// <param name="index">The index of the native string to get.</param>
	public string GetString(uint index) => IsDisposed
		? throw new ObjectDisposedException(nameof(NativeStringListUtf8))
		: (index >= Count)
			? throw new ArgumentOutOfRangeException()
			: Encoding.UTF8.GetString(new ReadOnlySpan<byte>(_data[index], (int)_lengths[index]));

	/// <summary>Returns an enumerator over the native strings converted into managed strings.</summary>
	public IEnumerator<string> GetEnumerator()
	{
		if (IsDisposed) throw new ObjectDisposedException(nameof(NativeStringListUtf8));
		for (uint i = 0; i < Count; ++i) {
			yield return GetString(i);
		}
	}

	IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


	/// <summary>Ensures that the list has at least the given capacity.</summary>
	/// <remarks>Will never shrink the list.</remarks>
	public void EnsureCapacity(uint capacity)
	{
		if (IsDisposed) throw new ObjectDisposedException(nameof(NativeStringListUtf8));
		if (_lengths.Length >= capacity) return;

		// Resize the lengths array
		Array.Resize(ref _lengths, (int)capacity);

		// Grow the native array, performing copies and frees as needed
		var newData = (byte**)NativeMemory.AllocZeroed(capacity, 8u);
		if (_data != null) {
			for (uint i = 0; i < Count; ++i) {
				newData[i] = _data[i];
			}
			NativeMemory.Free(_data);
		}
		_data = newData;
	}

	// Gets the next capacity given the current one
	private static uint NextCapacity(uint current) => current switch {
		<= 4  => 8,
		<= 64 => current * 2,
		_     => current * 3 / 2
	};


	/// <summary>Adds a new string to the end of the list.</summary>
	public void Add(string str) => Add(str.AsSpan());

	/// <summary>Adds a new string (as a char span) to the end of the list.</summary>
	public void Add(ReadOnlySpan<char> span)
	{
		if (IsDisposed) throw new ObjectDisposedException(nameof(NativeStringListUtf8));

		// Create native string
		var len = Encoding.UTF8.GetByteCount(span);
		var strData = NativeStringUtf8.AllocateNativeString((nuint)len + 1);
		Encoding.UTF8.GetBytes(span, new(strData, len));
		strData[len] = 0;

		// Add to array
		if (Count == _lengths.Length) {
			EnsureCapacity(NextCapacity((uint)_lengths.Length));
		}
		_data[Count] = strData;
		_lengths[Count++] = (uint)len;
	}

	/// <summary>Adds a new sequence of raw bytes to the end of the list.</summary>
	/// <remarks>Still adds a null-terminator to the byte sequence.</remarks>
	public void Add(ReadOnlySpan<byte> span)
	{
		if (IsDisposed) throw new ObjectDisposedException(nameof(NativeStringListUtf8));

		// Copy to native string
		var strData = NativeStringUtf8.AllocateNativeString((nuint)span.Length + 1);
		span.CopyTo(new(strData, span.Length));
		strData[span.Length] = 0;

		// Add to array
		if (Count == _lengths.Length) {
			EnsureCapacity(NextCapacity((uint)_lengths.Length));
		}
		_data[Count] = strData;
		_lengths[Count++] = (uint)span.Length;
	}

	/// <summary>Removes the string at the given index.</summary>
	public void RemoveAt(Index index)
	{
		if (IsDisposed) throw new ObjectDisposedException(nameof(NativeStringListUtf8));
		var realIndex = (uint)index.GetOffset((int)Count);
		if (realIndex >= Count) throw new ArgumentOutOfRangeException(nameof(index));

		// Free the string data
		NativeStringUtf8.FreeNativeString(_data[realIndex]);

		// Shift the remaining strings
		for (uint i = realIndex + 1; i < Count; ++i) {
			_data[i - 1] = _data[i];
			_lengths[i - 1] = _lengths[i];
		}
		Count -= 1;
	}

	/// <summary>Frees and removes all native strings from the list.</summary>
	public void Clear()
	{
		if (IsDisposed) throw new ObjectDisposedException(nameof(NativeStringListUtf8));

		for (uint i = 0; i < Count; ++i) {
			NativeStringUtf8.FreeNativeString(_data[i]);
		}
		Count = 0;
	}
}
