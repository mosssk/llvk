﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Buffers;
using System.Runtime.InteropServices;
using System.Text;

namespace LLVK;


/// <summary>
/// Contains a null-terminated, UTF-8 encoded C-string that is pre-pinned in memory (<c>const char*</c>).
/// </summary>
/// <remarks>
/// Uses natively allocated memory, be sure to call <see cref="Dispose"/>. Not thread-safe. Should never be modified
/// while in use by native code.
/// </remarks>
public sealed unsafe class NativeStringUtf8 : IDisposable, IEquatable<string>, IEquatable<NativeStringUtf8>
{
	#region Fields
	/// <summary>The length of the native string, in bytes.</summary>
	/// <remarks>Does not include the terminating null character.</remarks>
	public readonly uint Length;

	/// <summary>The native C-string data. Throws if the string is disposed.</summary>
	public ReadOnlySpan<byte> Data => !IsDisposed
		? new(_data, (int)Length)
		: throw new ObjectDisposedException(nameof(NativeStringUtf8));

	/// <summary>The raw pointer to the fixed string data. Memory at the pointer should not be modified.</summary>
	/// <remarks>The returned pointer is longer valid once the native string is disposed.</remarks>
	public byte* DataPtr => !IsDisposed
		? _data
		: throw new ObjectDisposedException(nameof(NativeStringUtf8));

	// The natively allocated string data
	private byte* _data;

	// The pre-calculated hash
	private readonly int _hash;

	/// <summary>If the string has been disposed.</summary>
	public bool IsDisposed => _data == null;
	#endregion // Fields


	/// <summary>Allocates an empty (zero-length) string.</summary>
	public NativeStringUtf8() : this(ReadOnlySpan<byte>.Empty) { }

	/// <summary>Allocates a new native string with the characters from the given string.</summary>
	public NativeStringUtf8(string str) : this(str.AsSpan()) { }

	/// <summary>Allocates a new native string with the characters from the given span.</summary>
	public NativeStringUtf8(ReadOnlySpan<char> span)
	{
		var len = Encoding.UTF8.GetByteCount(span);
		_data = AllocateNativeString((nuint)len + 1);
		Encoding.UTF8.GetBytes(span, new(_data, len));
		_data[len] = 0;
		Length = (uint)len;
		_hash = CalculateHash(new(_data, len));
	}

	/// <summary>Allocates a new native string that contains the given raw bytes.</summary>
	/// <remarks>Still adds a null-terminator to the end of the byte sequence.</remarks>
	public NativeStringUtf8(ReadOnlySpan<byte> span)
	{
		_data = AllocateNativeString((nuint)span.Length + 1);
		span.CopyTo(new(_data, span.Length));
		_data[span.Length] = 0;
		Length = (uint)span.Length;
		_hash = CalculateHash(span);
	}

	~NativeStringUtf8() => Dispose();

	public void Dispose()
	{
		if (_data != null) {
			FreeNativeString(_data);
			_data = null;
		}
		GC.SuppressFinalize(this);
	}


	/// <summary>Gets the contents of the native string as a managed string.</summary>
	/// <remarks>Use this instead of <see cref="ToString"/> to get the string contents.</remarks>
	public string GetString() => _data != null
		? Encoding.UTF8.GetString(new ReadOnlySpan<byte>(_data, (int)Length))
		: throw new ObjectDisposedException(nameof(NativeStringUtf8));


	#region Base
	public override int GetHashCode() => _hash;

	/// <summary>Note: does not return the string contents. Use <see cref="GetString"/>.</summary>
	public override string ToString() => $"NativeString[0x{(ulong)_data:X16}]";

	public override bool Equals(object? obj) =>
		(obj is string str && Equals(str)) || (obj is NativeStringUtf8 natStr && Equals(natStr));

	public bool Equals(string? str)
	{
		if (str is null) return false;
		var len = Encoding.UTF8.GetByteCount(str);
		if (len != Length) return false;
		if (Length == 0) return true;

		if (len <= 128) {
			Span<byte> strData = stackalloc byte[len];
			Encoding.UTF8.GetBytes(str.AsSpan(), strData);
			return Data.SequenceCompareTo(strData) == 0;
		}
		else {
			var strData = ArrayPool<byte>.Shared.Rent(len);
			try {
				Encoding.UTF8.GetBytes(str.AsSpan(), strData.AsSpan());
				return Data.SequenceCompareTo(strData.AsSpan(..len)) == 0;
			}
			finally {
				ArrayPool<byte>.Shared.Return(strData);
			}
		}
	}

	public bool Equals(NativeStringUtf8? str)
	{
		if (str is null || str.Length != Length || str._hash != _hash) return false;
		return Data.SequenceCompareTo(str.Data) == 0;
	}
	#endregion // Base


	public static bool operator == (NativeStringUtf8? l, string? r) => l?.Equals(r) ?? r is null;
	public static bool operator != (NativeStringUtf8? l, string? r) => !l?.Equals(r) ?? r is not null;

	public static bool operator == (NativeStringUtf8? l, NativeStringUtf8? r) => l?.Equals(r) ?? r is null;
	public static bool operator != (NativeStringUtf8? l, NativeStringUtf8? r) => !l?.Equals(r) ?? r is not null;


	// Allocates native memory for a string of the given byte length (null-terminator not included)
	internal static byte* AllocateNativeString(nuint length) => (byte*)NativeMemory.AlignedAlloc(length, 32u);

	// Frees native memory for a string
	internal static void FreeNativeString(byte* strPtr) => NativeMemory.AlignedFree(strPtr);


	private static int CalculateHash(ReadOnlySpan<byte> span)
	{
		HashCode hash = new();
		hash.AddBytes(span);
		return hash.ToHashCode();
	}
}
