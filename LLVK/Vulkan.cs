﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

// ReSharper disable InconsistentNaming

namespace LLVK;


/// <summary>
/// The root class containing the generated Vulkan API bindings. Contains the core (non-vendored) code.
/// </summary>
public static unsafe partial class Vulkan
{
	/// <summary><c>VK_MAKE_VERSION</c> macro (deprecated).</summary>
	[Obsolete("Use VK_MAKE_API_VERSION instead")]
	public static uint VK_MAKE_VERSION(uint major, uint minor, uint patch) => (major << 22) | (minor << 12) | patch;
	/// <summary><c>VK_VERSION_MAJOR</c> macro (deprecated).</summary>
	[Obsolete("Use VK_API_VERSION_MAJOR instead")]
	public static uint VK_VERSION_MAJOR(uint version) => version >> 22;
	/// <summary><c>VK_VERSION_MINOR</c> macro (deprecated).</summary>
	[Obsolete("Use VK_API_VERSION_MINOR instead")]
	public static uint VK_VERSION_MINOR(uint version) => (version >> 12) & 0x3FFU;
	/// <summary><c>VK_VERSION_PATCH</c> macro (deprecated).</summary>
	[Obsolete("Use VK_API_VERSION_PATCH instead")]
	public static uint VK_VERSION_PATCH(uint version) => version & 0xFFFU;

	/// <summary><c>VK_MAKE_API_VERSION</c> macro.</summary>
	public static uint VK_MAKE_API_VERSION(uint variant, uint major, uint minor, uint patch) =>
		(variant << 29) | (major << 22) | (minor << 12) | patch;
	/// <summary><c>VK_API_VERSION_VARIANT</c> macro.</summary>
	public static uint VK_API_VERSION_VARIANT(uint version) => version >> 29;
	/// <summary><c>VK_API_VERSION_MAJOR</c> macro.</summary>
	public static uint VK_API_VERSION_MAJOR(uint version) => (version >> 22) & 0x7FU;
	/// <summary><c>VK_API_VERSION_MINOR</c> macro.</summary>
	public static uint VK_API_VERSION_MINOR(uint version) => (version >> 12) & 0x3FFU;
	/// <summary><c>VK_API_VERSION_PATCH</c> macro.</summary>
	public static uint VK_API_VERSION_PATCH(uint version) => version & 0xFFFU;

	/// <summary><c>VK_API_VERSION</c> macro (deprecated).</summary>
	[Obsolete("Specific version defines (e.g. VK_API_VERSION_1_0) should be used instead")]
	public const uint VK_API_VERSION = 1u << 22; // 1.0.0

	/// <summary><c>VK_API_VERSION_1_0</c> version constant.</summary>
	public const uint VK_API_VERSION_1_0 = 1u << 22; // 1.0.0
	/// <summary><c>VK_API_VERSION_1_1</c> version constant.</summary>
	public const uint VK_API_VERSION_1_1 = (1u << 22) | (1u << 12); // 1.1.0
	/// <summary><c>VK_API_VERSION_1_2</c> version constant.</summary>
	public const uint VK_API_VERSION_1_2 = (1u << 22) | (2u << 12); // 1.2.0


	/// <summary>Boolean flag constructed from a 32-bit integer. Compatible with <c>VkBool32</c> C define.</summary>
	[StructLayout(LayoutKind.Explicit, Size = 4)]
	public readonly struct VkBool32 : IEquatable<VkBool32>, IComparable<VkBool32>, IEquatable<bool>
	{
		private const uint TRUE = 1, FALSE = 0;
		private static readonly int TrueHash = TRUE.GetHashCode(), FalseHash = FALSE.GetHashCode();

		/// <summary>True constant.</summary>
		public static readonly VkBool32 True = new(true);
		/// <summary>False constant.</summary>
		public static readonly VkBool32 False = new(false);

		[FieldOffset(0)] private readonly uint _value;

		/// <summary>Create from a bool value.</summary>
		public VkBool32(bool value) => _value = value ? TRUE : FALSE;
		/// <summary>Create from an integer value (non-zero is true).</summary>
		public VkBool32(uint value) => _value = (value == FALSE) ? FALSE : TRUE;

		public override string ToString() => _value == FALSE ? "0" : "1";
		public override int GetHashCode() => _value == FALSE ? FalseHash : TrueHash;

		public override bool Equals(object? obj) => obj is VkBool32 b && _value == b._value;
		public bool Equals(VkBool32 other) => _value == other._value;
		public bool Equals(bool other) => (_value == TRUE) == other;

		public int CompareTo(VkBool32 other) => _value.CompareTo(other._value);

		public static implicit operator bool (VkBool32 b) => b._value != FALSE;
		public static implicit operator VkBool32 (bool b) => new(b);
		public static implicit operator uint (VkBool32 b) => b._value;
		public static implicit operator VkBool32 (uint b) => new(b);

		public static bool operator == (VkBool32 l, VkBool32 r) => l._value == r._value;
		public static bool operator != (VkBool32 l, VkBool32 r) => l._value != r._value;

		public static VkBool32 operator & (VkBool32 l, VkBool32 r) => new(l._value & r._value);
		public static VkBool32 operator | (VkBool32 l, VkBool32 r) => new(l._value | r._value);
		public static VkBool32 operator ^ (VkBool32 l, VkBool32 r) => new(l._value ^ r._value);
	}


	/// <summary>Contains the instance-level Vulkan extensions as a set of boolean flags.</summary>
	public sealed partial class InstanceExtensionSet
	{
		/// <summary>Gets if the given extension is <c>true</c> in the set.</summary>
		/// <param name="extName">The name of the instance-level extension to check.</param>
		/// <exception cref="ArgumentException">The given name is not a valid instance-level extension.</exception>
		public partial bool Get(string extName);

		/// <summary>Sets or clears the given extension in the set.</summary>
		/// <param name="extName">The name of the instance-level extension to update.</param>
		/// <param name="isSet">If the extension should be set or cleared.</param>
		/// <returns>The value of <paramref name="isSet"/>.</returns>
		public partial bool Set(string extName, bool isSet);

		/// <summary>Enumerates over the names of the extensions that are enabled in the set.</summary>
		public partial IEnumerable<string> EnumerateSet();
	}


	/// <summary>Contains the device-level Vulkan extensions as a set of boolean flags.</summary>
	public sealed partial class DeviceExtensionSet
	{
		/// <summary>Gets if the given extension is <c>true</c> in the set.</summary>
		/// <param name="extName">The name of the device-level extension to check.</param>
		/// <exception cref="ArgumentException">The given name is not a valid device-level extension.</exception>
		public partial bool Get(string extName);

		/// <summary>Sets or clears the given extension in the set.</summary>
		/// <param name="extName">The name of the device-level extension to update.</param>
		/// <param name="isSet">If the extension should be set or cleared.</param>
		/// <returns>The value of <paramref name="isSet"/>.</returns>
		public partial bool Set(string extName, bool isSet);

		/// <summary>Enumerates over the names of the extensions that are enabled in the set.</summary>
		public partial IEnumerable<string> EnumerateSet();
	}

	/// <summary>Contains the instance-level Vulkan layers as a list of layer descriptions.</summary>
	public sealed partial class InstanceLayerSet
	{
		/// <summary>Represents a single Vulkan layer.</summary>
		/// <param name="Name">The name of the layer.</param>
		/// <param name="Version">The layer implementation version.</param>
		public readonly record struct Layer(string Name, uint Version);

		/// <summary>The list of all layers.</summary>
		public IReadOnlyList<Layer> Layers => _layers;
		private readonly Layer[] _layers;

		private InstanceLayerSet(Layer[] layers) => _layers = layers;

		/// <summary>Gets if the layer with the given name is present in the set.</summary>
		/// <param name="layerName">The layer name to check for.</param>
		public bool Has(string layerName)
		{
			foreach (var layer in Layers) {
				if (layer.Name == layerName) return true;
			}
			return false;
		}
	}


	static Vulkan()
	{
		// Static check for 64-bit LE environment
		if (sizeof(nuint) != 8 || !BitConverter.IsLittleEndian) {
			throw new PlatformNotSupportedException("LLVK must be run on a 64-bit little-endian system");
		}

		// Load the Vulkan library and global functions
		VulkanLoader.LoadLibraryAndGlobalFunctions();
	}
}
