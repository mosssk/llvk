﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace LLVK;


/// <summary>Provides the functionality for loading instance-level and device-level Vulkan API functions.</summary>
public static unsafe partial class VulkanLoader
{
	/// <summary>If the loader has been disabled using the <c>_LLVK_DISABLE_LOADER</c> environment variable.</summary>
	public static readonly bool IsLoaderDisabled =
		Environment.GetEnvironmentVariable("_LLVK_DISABLE_LOADER") is not (null or "0");


	/// <summary>Loads the instance-level functions for the given instance handle.</summary>
	/// <param name="handle">The instance to load functions for.</param>
	public static partial void LoadInstanceFunctions(Vulkan.VkInstance handle);

	/// <summary>Loads the device-level functions for the given device handle.</summary>
	/// <param name="handle">The device to load functions for.</param>
	public static partial void LoadDeviceFunctions(Vulkan.VkDevice handle);

	// Loads the library and global functions
	internal static partial void LoadLibraryAndGlobalFunctions();


	/// <summary>Checks if the given Vulkan API function is loaded.</summary>
	/// <param name="funcName">The name of the API function to check.</param>
	/// <exception cref="ArgumentException">The given function name is not a known Vulkan function.</exception>
	public static bool IsFunctionLoaded(string funcName) => GetFunctionPointer(funcName) != null;

	/// <summary>Gets the unmanaged function pointer to the given function, or <c>null</c> if not loaded.</summary>
	/// <param name="funcName">The name of the API function to get the unmanaged pointer to.</param>
	/// <exception cref="ArgumentException">The given function name is not a known Vulkan function.</exception>
	public static partial void* GetFunctionPointer(string funcName);


	/// <summary>The handle to the loaded Vulkan dynamic library instance.</summary>
	/// <remarks>Will be <c>IntPtr.Zero</c> if the library has not been statically loaded yet.</remarks>
	public static IntPtr LibraryHandle { get; private set; }


	/// <summary>
	/// Provides an enumerator over the expected Vulkan dynamic library file names for the current platform.
	/// </summary>
	public static IEnumerable<string> EnumerateLibraryNames()
	{
		if (OperatingSystem.IsWindows()) {
			yield return "vulkan-1.dll"; // Common name
			yield return "vulkan.dll";   // Unlikely name
		}
		else if (OperatingSystem.IsMacOS() || OperatingSystem.IsMacCatalyst() || OperatingSystem.IsIOS()) {
			// NOT WatchOS or tvOS
			yield return "libMoltenVK.dylib"; // MoltenVK
			yield return "libVulkan.dylib";   // Common other name (unlikely)
		}
		else if (OperatingSystem.IsLinux() || OperatingSystem.IsAndroid()) {
			yield return "libvulkan.so.1";
			yield return "libvulkan.so";
		}
		else if (OperatingSystem.IsFreeBSD()) {
			yield return "libvulkan.so.1";
			yield return "libvulkan.so";
		}
	}


	// Populates a byte span with UTF data from a string and ends with a null byte
	[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
	internal static unsafe void PopulateNameBuffer(string str, byte* buffer, int bufferLen)
	{
		var len = Encoding.UTF8.GetBytes(str, new(buffer, bufferLen));
		if (len >= bufferLen - 1) {
			throw new InvalidOperationException("Failed to populate string buffer (THIS IS AN LLVK BUG)");
		}
		buffer[len] = 0;
	}
}
