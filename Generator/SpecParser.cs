﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace LLVK;


// Implements parsing for the Vulkan XML spec
//    Remember: the default reaction to *anything* that isn't explicitly handled is to throw an error so the developers
//    know that there is something new that needs to be explicitly handled.
internal static class SpecParser
{
	// Regex for getting the header version from the VK_HEADER_VERSION define
	private static readonly Regex _HeaderVersionRegex = new(
		@"#define\s+VK_HEADER_VERSION\s+(\d+)\s*$", RegexOptions.Singleline
	);
	// Regex for finding the spec version from the VK_HEADER_VERSION_COMPLETE define
	private static readonly Regex _SpecVersionRegex = new(
		@"VK_MAKE_API_VERSION\s*\(\s*0,\s*(\d+),\s*(\d+),\s*VK_HEADER_VERSION\s*\)$", RegexOptions.Singleline
	);
	// Regex for negated API constants (all follow the same form)
	private static readonly Regex _NegatedApiConstantRegex = new(
		@"^\(~(\d+)(?:U|UL|ULL)\)$", RegexOptions.Singleline
	);
	// Regex for "_RESERVED_XXX_BIT_" enum fields, which are ignored
	private static readonly Regex _ReservedBitRegex = new(
		@"_RESERVED_\d+_BIT_", RegexOptions.Singleline | RegexOptions.Compiled
	);
	// Regex for funcpointer return type
	private static readonly Regex _FuncPointerRetRegex = new(
		@"^typedef\s+(\w+\s*?\**)\s+\(VKAPI_PTR", RegexOptions.Singleline
	);
	// Regex for funcpointer parameters
	private static readonly Regex _FuncPointerArgRegex = new(
		@"(\w+\s*?\**)\s+(\w+)\s*[,)]", RegexOptions.Singleline
	);
	// Regex for array specifiers in struct fields
	private static readonly Regex _FieldArrayRegex = new(
		@"\[(.*?)\]", RegexOptions.Singleline | RegexOptions.Compiled
	);
	// Regex for struct member bitfield detection
	private static readonly Regex _FieldBitfieldRegex = new(
		@":(\d+)$", RegexOptions.Singleline | RegexOptions.Compiled
	);

	// Defines that are known to be statically handled
	private static readonly IReadOnlySet<string> _KnownDefines = new HashSet<string> {
		"VK_MAKE_VERSION", "VK_VERSION_MAJOR", "VK_VERSION_MINOR", "VK_VERSION_PATCH",
		"VK_MAKE_API_VERSION", "VK_API_VERSION_VARIANT", "VK_API_VERSION_MAJOR", "VK_API_VERSION_MINOR",
		"VK_API_VERSION_PATCH",
		"VK_API_VERSION", "VK_API_VERSION_1_0", "VK_API_VERSION_1_1", "VK_API_VERSION_1_2",
		"VK_DEFINE_HANDLE", "VK_USE_64_BIT_PTR_DEFINES", "VK_NULL_HANDLE", "VK_DEFINE_NON_DISPATCHABLE_HANDLE"
	};


	#region Utilities
	private static int GetLine(XObject obj) => (obj is IXmlLineInfo li && li.HasLineInfo()) ? li.LineNumber : -1;

	private static string GetSubtext(XElement e) => e.DescendantNodes()
		.OfType<XText>()
		.Where(static xt => xt.Parent?.Name != "comment")
		.Aggregate(new StringBuilder(), (s, t) => s.Append(t.Value), s => s.ToString());

	private static SpecError Error(string msg) => new(msg);

	private static SpecError Error(string msg, XObject obj) =>
		(GetLine(obj) is var line && line >= 0) ? new($"{msg} (at line {line})") : new(msg);
	#endregion // Utilities


	// Top level parsing entry point for the spec file
	public static async Task<VulkanApi> ParseAsync(Version specVersion, FileInfo specFile, CancellationToken token)
	{
		var api = new VulkanApi(specVersion);

		// Read and parse the spec file
		XElement xml;
		try {
			await using var fileStream = specFile.OpenRead();
			var doc = await XDocument.LoadAsync(fileStream, LoadOptions.SetLineInfo, token);
			xml = doc.XPathSelectElement("/registry") ??
				throw new SpecError("Spec file does not appear to be the Vulkan XML spec");
			token.ThrowIfCancellationRequested();
		}
		catch (IOException ex) {
			throw new SpecError($"Failed to read spec file - {ex.Message}", ex);
		}
		catch (XmlException ex) {
			throw new SpecError($"Spec file is invalid (at {ex.LineNumber}:{ex.LinePosition}) - {ex.Message}", ex);
		}
		catch (InvalidOperationException ex) {
			throw new SpecError($"Spec file is invalid - {ex.Message}", ex);
		}

		// Defines, vendors, and constants first since the rest of the parsing relies heavily on these components
		ParseDefines(api, xml);
		token.ThrowIfCancellationRequested();
		ParseVendors(xml);
		token.ThrowIfCancellationRequested();
		ParseApiConstants(api, xml);
		token.ThrowIfCancellationRequested();
		await Task.Yield();

		// Parse the base types
		ParseBaseTypes(api, xml);
		token.ThrowIfCancellationRequested();
		await Task.Yield();

		// Parse the extensions and enabled types/fields
		ParseExtensionsAndTypes(api, xml, token);
		token.ThrowIfCancellationRequested();
		await Task.Yield();

		// Parse the enums
		ParseEnums(api, xml, token);
		token.ThrowIfCancellationRequested();
		await Task.Yield();

		// Parse the function pointers
		ParseFunctionPointers(api, xml);
		token.ThrowIfCancellationRequested();
		await Task.Yield();

		// Parse the handles
		ParseHandles(api, xml, token);
		token.ThrowIfCancellationRequested();
		await Task.Yield();

		// Parse the structs
		ParseStructs(api, xml, token);
		token.ThrowIfCancellationRequested();
		await Task.Yield();

		// Parse the commands
		ParseFunctions(api, xml, token);
		token.ThrowIfCancellationRequested();
		await Task.Yield();

		// Check that all expected features were handled
		if (api.FeatureTypes.Count > 0) {
			var missing = String.Join(", ", api.FeatureTypes.Keys);
			throw Error($"Features were not handled: {missing}");
		}
		if (api.FeatureCommands.Count > 0) {
			var missing = String.Join(", ", api.FeatureCommands.Keys);
			throw Error($"Commands were not handled: {missing}");
		}

		return api;
	}


	// Parse the defines
	//    Defines are unique because they can be just about anything (they are equivalent to C macros). Because of this,
	//    each define must be handed manually using logic below. Each define will need to be handled differently, e.g.
	//    some are constant integers, some become functions, and some are parsed to figure out the API version. To
	//    ensure this, all defines that are not explicitly handled generate an unrecoverable error, and must be manually
	//    dealt with as they appear in new versions (the base Vulkan.cs file in LLVK is a good place to add define
	//    functionality as needed).
	private static void ParseDefines(VulkanApi api, XNode root)
	{
		Log.Info("Validating define types...");

		int major = -1, minor = -1, header = -1;

		// Search
		var defineList = root.XPathSelectElements("types/type[@category='define']");
		foreach (var def in defineList) {
			// Defines must have a <name> child or attr
			var name =
				def.Elements().FirstOrDefault(static e => e.Name == "name")?.Value ??
				def.Attribute("name")?.Value ??
				throw Error("Define type does not have a name", def);

			// This section handles defines that we must respond to dynamically based on value
			switch (name)
			{
			case "VK_HEADER_VERSION": {
				if (_HeaderVersionRegex.Match(GetSubtext(def)).Groups is not { Count: 2 } groups) {
					throw Error("VK_HEADER_VERSION could not be parsed");
				}
				header = Int32.Parse(groups[1].Value);
			} continue;
			case "VK_HEADER_VERSION_COMPLETE": {
				if (_SpecVersionRegex.Match(GetSubtext(def)).Groups is not { Count: 3 } groups) {
					throw Error("VK_HEADER_VERSION_COMPLETE could not be parsed");
				}
				major = Int32.Parse(groups[1].Value);
				minor = Int32.Parse(groups[2].Value);
			} continue;
			}

			// This section handles static defines that we know about and are handled correctly
			if (_KnownDefines.Contains(name)) continue;

			// Report an unknown define
			throw Error($"Unknown define: {name}", def);
		}

		// Check
		if (major == -1 || minor == -1 || header == -1) throw Error("Could not find the API version");
		Version foundVersion = new(major, minor, header);
		if (api.SpecVersion != foundVersion) {
			throw Error(
				$"Spec version was not the expected value (expected: {api.SpecVersion}, actual: {foundVersion})");
		}

		Log.Info($"All defines validated (API version: {api.SpecVersion})");
	}


	// Parse the vendors
	//    Vendors are used to separate the types and functions into categories. Their tags should always be all caps.
	private static void ParseVendors(XNode root)
	{
		Log.Info("Searching for vendors...");

		var vendors = VulkanApi.KnownVendors.Keys.ToHashSet();

		// Search
		var nameList = root.XPathSelectElements("tags/tag");
		foreach (var nameElem in nameList) {
			var name = nameElem.Attribute("name");
			if (name is null) throw Error("Found a vendor without a name", nameElem);
			if (!name.Value.All(static ch => Char.IsUpper(ch))) throw Error("Found non-caps vendor tag", nameElem);
			if (!vendors.Remove(name.Value)) {
				throw Error($"Unknown vendor '{name.Value}'", nameElem);
			}
			Log.Verbose($"   Found vendor '{name.Value}'");
		}

		// Check removed
		if (vendors.Count > 0) {
			throw Error($"Potentially removed vendor(s): {String.Join(", ", vendors)}");
		}

		Log.Info($"Found {VulkanApi.KnownVendors.Count} vendors");
	}


	// Parse the API constants (appearing in their own "API Constants" enum)
	private static void ParseApiConstants(VulkanApi api, XNode root)
	{
		Log.Info("Parsing API constants...");

		var constList = root.XPathSelectElement("enums[@name='API Constants']") ??
			throw Error("Could not find API constants");
		foreach (var elem in constList.Elements("enum")) {
			var name = elem.Attribute("name")?.Value ?? throw Error("Found constant without name", elem);

			// Check alias fist
			if (elem.Attribute("alias") is { Value: {} alias }) {
				if (!api.Constants.TryGetValue(alias, out var aConst)) {
					throw Error($"Unknown constant alias '{alias}'", elem);
				}
				api.Constants.Add(name, aConst with { Name = name });
				continue;
			}

			// Switch based on declared type
			if (elem.Attribute("type") is not { Value: { } type }) throw Error("Missing constant type", elem);
			if (elem.Attribute("value") is not { Value: { } value }) throw Error("Missing constant value", elem);
			switch (type)
			{
			case "uint32_t": {
				if (!UInt32.TryParse(value, out var u32)) {
					if (_NegatedApiConstantRegex.Match(value) is not { Groups.Count: 2 } negMatch ||
						!UInt32.TryParse(negMatch.Groups[1].Value, out u32)) {
						throw Error($"Unknown uint32_t constant value '{value}'", elem);
					}
					u32 = ~u32;
				}
				api.Constants.Add(name, new(name, "uint", $"0x{u32:X8}", value));
			} break;
			case "uint64_t": {
				if (!UInt64.TryParse(value, out var u64)) {
					if (_NegatedApiConstantRegex.Match(value) is not { Groups.Count: 2 } negMatch ||
						!UInt64.TryParse(negMatch.Groups[1].Value, out u64)) {
						throw Error($"Unknown uint64_t constant value '{value}'", elem);
					}
					u64 = ~u64;
				}
				api.Constants.Add(name, new(name, "ulong", $"0x{u64:X16}", value));
			} break;
			case "float": {
				var len = value.EndsWith('F') ? value.Length - 1 : value.Length;
				if (!Single.TryParse(value.AsSpan(0, len), out var f32)) {
					throw Error($"Unknown float constant value '{value}'", elem);
				}
				api.Constants.Add(name, new(name, "float", $"{f32:G9}", value)); // G9 recommended for "round-trip"
			} break;
			default: throw Error($"Unknown constant type '{type}'", elem);
			}

			// Verbose report
			Log.Verbose($"   Found API constant '{name}' = {api.Constants[name].Value}");
		}

		Log.Info($"Found {api.Constants.Count} constants");
	}


	// Parse the base types and require types
	//    These are primarily platform-specific types, C stdlib types, and core Vulkan typedefs
	private static void ParseBaseTypes(VulkanApi api, XNode root)
	{
		Log.Info("Checking the base types...");

		api.BaseTypes.Add("int", new ApiType.Numeric("int", "int", 4)); // Not picked up from header logic
		uint skippedVideo = 0;

		// Search ("requires" == specific types included from system and platform headers)
		var requireList = root.XPathSelectElements("types/type[@requires][not(@category)]");
		foreach (var (typeElem, nameAttr) in requireList.Select(static e => (e, e.Attribute("name")))) {
			if (nameAttr is not { Value: { } name }) throw Error("Found required type without name", typeElem);
			if (api.TryGetType(name, out _)) throw Error($"Duplicate type found: '{name}'", nameAttr);
			if (name.StartsWith("StdVideo")) { // Skip incomplete video beta
				skippedVideo += 1;
				continue;
			}

			api.BaseTypes.Add(name, name switch {
				"Display"             => new ApiType.Special("Display", "void", 0),
				"VisualID"            => new ApiType.Special("VisualID", "ulong", 8),
				"Window"              => new ApiType.Special("Window", "ulong", 8),
				"RROutput"            => new ApiType.Special("RROutput", "ulong", 8),
				"wl_display"          => new ApiType.Special("wl_display", "void", 0),
				"wl_surface"          => new ApiType.Special("wl_surface", "void", 0),
				// IntPtr used instead of nuint
				"HINSTANCE"           => new ApiType.Special("HINSTANCE", "IntPtr", 8),
				"HWND"                => new ApiType.Special("HWND", "IntPtr", 8),
				"HMONITOR"            => new ApiType.Special("HMONITOR", "IntPtr", 8),
				"HANDLE"              => new ApiType.Special("HANDLE", "IntPtr", 8),
				"SECURITY_ATTRIBUTES" => new ApiType.Special("SECURITY_ATTRIBUTES", "void", 0),
				"DWORD"               => new ApiType.Special("DWORD", "uint", 4),
				"LPCWSTR"             => new ApiType.Pointer(new ApiType.Numeric("wchar_t", "short", 2), 1),
				"xcb_connection_t"    => new ApiType.Special("xcb_connection_t", "void", 0),
				"xcb_visualid_t"      => new ApiType.Special("xcb_visualid_t", "uint", 4),
				"xcb_window_t"        => new ApiType.Special("xcb_window_t", "uint", 4),
				"IDirectFB"           => new ApiType.Special("IDirectFB", "void", 0),
				"IDirectFBSurface"    => new ApiType.Special("IDirectFBSurface", "void", 0),
				"zx_handle_t"         => new ApiType.Special("zx_handle_t", "uint", 4),
				"GgpStreamDescriptor" => new ApiType.Special("GgpStreamDescriptor", "uint", 4),
				"GgpFrameToken"       => new ApiType.Special("GgpFrameToken", "uint", 4),
				"_screen_context"     => new ApiType.Special("_screen_context", "void", 0),
				"_screen_window"      => new ApiType.Special("_screen_window", "void", 0),
				"void"                => new ApiType.Special("void", "void", 0),
				"char"                => new ApiType.Numeric("char", "byte", 1),
				"float"               => new ApiType.Numeric("float", "float", 4),
				"double"              => new ApiType.Numeric("double", "double", 8),
				"int8_t"              => new ApiType.Numeric("int8_t", "sbyte", 1),
				"uint8_t"             => new ApiType.Numeric("uint8_t", "byte", 1),
				"int16_t"             => new ApiType.Numeric("int16_t", "short", 2),
				"uint16_t"            => new ApiType.Numeric("uint16_t", "ushort", 2),
				"int32_t"             => new ApiType.Numeric("int32_t", "int", 4),
				"uint32_t"            => new ApiType.Numeric("uint32_t", "uint", 4),
				"int64_t"             => new ApiType.Numeric("int64_t", "long", 8),
				"uint64_t"            => new ApiType.Numeric("uint64_t", "ulong", 8),
				"size_t"              => new ApiType.Numeric("size_t", "nuint", 8),
				_                     => throw Error($"Unknown required type '{name}'", nameAttr)
			});

			Log.Verbose($"   Found base type '{name}'");
		}

		// Search ("basetype" == typedefs for core Vulkan types)
		var baseTypeList = root.XPathSelectElements("types/type[@category='basetype']");
		foreach (var (typeElem, nameElem) in baseTypeList.Select(static e => (e, e.Element("name")))) {
			if (nameElem is not { Value: { } name }) throw Error("Found base type without name", typeElem);
			if (api.TryGetType(name, out _)) throw Error($"Duplicate type found: '{name}'", nameElem);

			api.BaseTypes.Add(name, name switch {
				"ANativeWindow"   => new ApiType.Special("ANativeWindow", "void", 0),
				"AHardwareBuffer" => new ApiType.Special("AHardwareBuffer", "void", 0),
				"CAMetalLayer"    => new ApiType.Special("CAMetalLayer", "void", 0),
				"VkSampleMask"    => new ApiType.Numeric("VkSampleMask", "uint", 4),
				"VkBool32"        => new ApiType.Numeric("VkBool32", "VkBool32", 4), // Hand-crafted
				"VkFlags"         => new ApiType.Special("VkFlags", "uint", 4),
				"VkFlags64"       => new ApiType.Special("VkFlags64", "ulong", 8),
				"VkDeviceSize"    => new ApiType.Numeric("VkDeviceSize", "ulong", 8),
				"VkDeviceAddress" => new ApiType.Numeric("VkDeviceAddress", "ulong", 8),
				_                 => throw Error($"Unknown base type '{name}'", nameElem)
			});

			Log.Verbose($"   Found base type '{name}'");
		}

		Log.Info($"Found {api.BaseTypes.Count} base types (skipped {skippedVideo} video types)");
	}


	// Parse the extensions and types/enum fields (types/fields are parsed here to remove beta or disabled ones)
	private static void ParseExtensionsAndTypes(VulkanApi api, XNode root, CancellationToken token)
	{
		Log.Info("Parsing core features and extensions...");

		// Set of hard-coded features to skip as they are handled explicitly
		var skippedFeatures = new HashSet<string> {
			"vk_platform", "VkBool32", "VkDeviceAddress", "VkDeviceSize", "VkFlags", "VkSampleMask",
			"ANativeWindow", "AHardwareBuffer", "CAMetalLayer"
		};

		// Get the list of all core feature sets and extensions
		var featureSetList = root.XPathSelectElements("feature[@api='vulkan']")
			.Concat(root.XPathSelectElements("extensions/extension"));
		foreach (var fSetElem in featureSetList) {
			if (fSetElem.Attribute("name") is not { Value: { } setName }) {
				throw Error("Feature set missing name", fSetElem);
			}

			// Skip disabled or provisional extensions
			var ignored = fSetElem.Attribute("supported")?.Value == "disabled" ||
				fSetElem.Attribute("provisional")?.Value == "true";
			if (ignored) {
				Log.Verbose($"   Skipping disabled or provisional extension '{setName}'");
			}

			// Get the core feature set, or extract the extension
			VulkanApi.FeatureSet? featureSet = ignored ? null : (fSetElem.Name == "extension")
				? parseAndCreateExtension(fSetElem)
				: fSetElem.Attribute("name")?.Value switch {
					"VK_VERSION_1_0" => VulkanApi.Core10,
					"VK_VERSION_1_1" => VulkanApi.Core11,
					"VK_VERSION_1_2" => VulkanApi.Core12,
					var badName      => throw Error($"Unknown or null core feature set '{badName}'", fSetElem)
				};
			if (!ignored && featureSet is VulkanApi.FeatureSet.Ext ext) {
				api.Extensions.Add(ext.Name, ext);
				Log.Verbose($"   Found ext '{ext.Name}' " +
					$"(ven={ext.Vendor}, #={ext.Number}, n='{ext.ExtNameValue}', ver={ext.ExtVersionValue})");
			}

			// Read the feature set types
			var typeList = fSetElem.XPathSelectElements("require/type");
			foreach (var typeElem in typeList) {
				var name = typeElem.Attribute("name")?.Value ?? throw Error("Type without name", typeElem);
				if (name.StartsWith("VK_")) continue; // Skip macros, which are given as types
				if (skippedFeatures.Remove(name)) continue; // Skip explicitly handled features
				if (ignored) api.DisabledFeatures.Add(name);
				else api.FeatureTypes[name] = featureSet!;
			}

			// Read the feature set commands
			var commandList = fSetElem.XPathSelectElements("require/command");
			foreach (var commandElem in commandList) {
				var name = commandElem.Attribute("name")?.Value ?? throw Error("Command without name", commandElem);
				if (ignored) api.DisabledFeatures.Add(name);
				else api.FeatureCommands[name] = featureSet!;
			}

			token.ThrowIfCancellationRequested();
		}

		Log.Info($"Found {api.Extensions.Count} extensions (skipped {api.DisabledFeatures.Count} features)");
		return;

		// Creates an extension
		static VulkanApi.FeatureSet.Ext parseAndCreateExtension(XElement xml)
		{
			// Get info
			var name = xml.Attribute("name")?.Value ?? throw Error("Extension without name", xml);
			var numberRaw = xml.Attribute("number")?.Value ?? throw Error("Extension without number", xml);
			if (!UInt32.TryParse(numberRaw, out var number)) {
				throw Error($"Extension number '{numberRaw}' could not be parsed", xml);
			}

			// Lookup vendor based on name
			var vendorName = name.Split('_', 3)[1];
			if (!VulkanApi.KnownVendors.TryGetValue(vendorName, out var vendor)) {
				throw Error($"Cannot extract vendor from extension '{name}'", xml);
			}

			// Get enum name string
			string? extNameName, extNameValue;
			var extNameElem = xml.XPathSelectElements("require/enum")
				.FirstOrDefault(static e => (e.Attribute("name")?.Value ?? "").EndsWith("_EXTENSION_NAME")) ??
				throw Error("Extension does not have name enum", xml);
			if (extNameElem.Attribute("alias")?.Value is { } aliasRaw) {
				if (xml.XPathSelectElement($"require/enum[@name='{aliasRaw}']") is not { } alias) {
					throw Error($"Extension missing name alias '{aliasRaw}'", extNameElem);
				}
				extNameName = extNameElem.Attribute("name")?.Value ?? String.Empty;
				extNameValue = alias.Attribute("value")!.Value;
			}
			else {
				extNameName = extNameElem.Attribute("name")?.Value ?? String.Empty;
				extNameValue = extNameElem.Attribute("value")?.Value.Trim('\"') ??
					throw Error("Extension missing name value", extNameElem);
			}

			// Get enum spec version
			var extVerElem = xml.XPathSelectElements("require/enum")
				.FirstOrDefault(static e => (e.Attribute("name")?.Value ?? "").EndsWith("_SPEC_VERSION")) ??
				throw Error("Extension does not have name enum", xml);
			var versionRaw = extVerElem.Attribute("value")?.Value ??
				throw Error("Extension missing version value", extVerElem);
			if (!UInt32.TryParse(versionRaw, out var extVersionValue)) {
				throw Error($"Extension version value '{versionRaw}' cannot be parsed", extVerElem);
			}
			var extVersionName = extVerElem.Attribute("name")?.Value ?? String.Empty;

			// Get instance/device type
			var type = xml.Attribute("type")?.Value ?? throw Error($"Extension '{name}' does not have type", xml);
			var isDevice = type switch {
				"device" => true,
				"instance" => false,
				_ => throw Error($"Unknown extension type '{type}'", xml)
			};

			// Return
			return new(name, number, extNameName, extNameValue, extVersionName, extVersionValue, isDevice, vendor);
		}
	}


	// Parses the enums and bitmasks
	private static void ParseEnums(VulkanApi api, XNode root, CancellationToken token)
	{
		Log.Info("Parsing bitmasks and enums");

		var skipped = 0;

		// Read the bitmask and enum types
		var enumTypeList = root.XPathSelectElements("types/type[@category='bitmask' or @category='enum']");
		foreach (var enumElem in enumTypeList) {
			var isMask = enumElem.Attribute("category")!.Value == "bitmask";

			// Extract name info and check against enabled types
			var name = enumElem.Attribute("name")?.Value ?? enumElem.Element("name")?.Value ??
				throw Error("Enum without name", enumElem);
			if (api.DisabledFeatures.Contains(name)) {
				Log.Verbose($"   Skipping disabled enum type '{name}'");
				skipped += 1;
				continue;
			}
			var (vendor, fieldRoot) = VulkanApi.ExtractEnumTypeInfo(name, isMask);
			if (!isMask && name.Contains("FlagBits") && name.EndsWith(vendor?.Tag ?? "")) {
				if (!api.FeatureTypes.ContainsKey(name.Replace("FlagBits", "Flags"))) {
					throw Error($"Cannot find matching flags for '{name}'", enumElem);
				}
				api.FeatureTypes.Remove(name);
				Log.Verbose($"   Skipping flag bits type '{name}'");
				skipped += 1;
				continue;
			}

			// Check for alias
			if (enumElem.Attribute("alias") is { Value: { } aliasRaw }) {
				if (!api.Enums.TryGetValue(aliasRaw, out var alias)) {
					throw Error($"Unknown alias '{aliasRaw}' for type '{name}'", enumElem);
				}
				api.Enums.Add(name, new(name, fieldRoot, alias) { Vendor = vendor });
				Log.Verbose($"   Found enum alias '{name}' -> '{alias.SpecName}'");
				continue;
			}

			// Parse the type info
			var isWide = enumElem.Element("type")?.Value == "VkFlags64";

			// Create the enum
			api.Enums.Add(name, new(name, fieldRoot, isMask, isWide) { Vendor = vendor });
			Log.Verbose($"   Found enum '{name}' (wide={isWide})");
		}
		token.ThrowIfCancellationRequested();

		// Read the enum fields
		var enumFieldList = root.XPathSelectElements("enums/enum")
			.Concat(root.XPathSelectElements("feature/require/enum[@extends]"))
			.Concat(root.XPathSelectElements("extensions/extension/require/enum[@extends]"));
		var missingAliases = new HashSet<ApiType.Enums>(); // Enums with unmatched alias fields
		foreach (var fieldElem in enumFieldList) {
			// Get the name
			var name = fieldElem.Attribute("name")?.Value ?? throw Error("Enum field without name", fieldElem);
			if (_ReservedBitRegex.IsMatch(name)) continue;

			// Check if disabled as a beta extension
			if (fieldElem.Attribute("protect")?.Value is "VK_ENABLE_BETA_EXTENSIONS" ||
				fieldElem.Parent?.Parent?.Attribute("supported")?.Value is "disabled" ||
				fieldElem.Parent?.Parent?.Attribute("provisional")?.Value is "true") continue;

			// Get the parent enum
			var parentRaw = (fieldElem.Parent?.Attribute("name")?.Value ??
				fieldElem.Attribute("extends")?.Value ??
				throw Error($"Cannot identify parent enum for field '{name}'", fieldElem)).Replace("FlagBits", "Flags");
			if (parentRaw == "API Constants") continue;
			if (api.DisabledFeatures.Contains(parentRaw)) continue;
			if (!api.Enums.TryGetValue(parentRaw, out var parent)) {
				throw Error($"Unknown parent enum '{parentRaw}' for field '{name}'", fieldElem);
			}

			// Parse the field
			var field = extractField(parent, fieldElem);

			// Create and add the field
			if (!parent.Fields.TryAdd(field.OutputName, field)) {
				var existing = parent.Fields[field.OutputName];
				if (field.Value != existing.Value) {
					throw Error($"Non-equal duplicate fields: '{field.SpecName}', '{existing.SpecName}'", fieldElem);
				}
			}
			if (field.Value is null) {
				missingAliases.Add(parent);
			}
		}
		token.ThrowIfCancellationRequested();

		// Resolve the missing aliases
		foreach (var @enum in missingAliases) {
			foreach (var (badName, badValue) in @enum.Fields.Where(static f => f.Value.Value is null)) {
				if (!@enum.Fields.TryGetValue(badValue.Alias!, out var alias)) {
					throw Error($"Unknown alias target '{badValue.Alias}' for value '{badValue.SpecName}'");
				}
				@enum.Fields[badName] = badValue with { Value = alias.Value };
			}
		}

		// Remove located enum types from the unassigned feature sets
		foreach (var @enum in api.Enums.Values) {
			api.FeatureTypes.Remove(@enum.SpecName);
		}

		Log.Info($"Found {api.Enums.Count} enums and bitmasks (skipped {skipped} ignored)");
		return;

		// Extracts the value of an enum field element
		static ApiType.Enums.Field extractField(ApiType.Enums parent, XElement xml)
		{
			// Extract name
			var name = xml.Attribute("name")?.Value ?? throw Error("Enum value does not have name", xml);
			var outName = VulkanApi.NormalizeEnumValueName(parent, name);

			// Handle aliases first
			if (xml.Attribute("alias") is { Value: { } alias }) {
				alias = VulkanApi.NormalizeEnumValueName(parent, alias);
				return new(name, outName, parent.Fields.TryGetValue(alias, out var aField) ? aField.Value : null, alias);
			}

			// Extract the value (many ways these are specified)
			long value;
			if (xml.Attribute("value") is { Value: { } rawValue }) {
				var (numKind, valueStr) = (rawValue.StartsWith("0x") || rawValue.StartsWith("0X"))
					? (NumberStyles.HexNumber, rawValue[2..]) : (NumberStyles.Number, rawValue);
				if (!Int64.TryParse(valueStr, numKind, null, out var i64)) {
					throw Error($"Failed to parse enum value '{rawValue}'", xml);
				}
				value = i64;
			}
			else if (xml.Attribute("bitpos") is { Value: { } bitposRaw }) {
				if (!UInt64.TryParse(bitposRaw, out var u64)) {
					throw Error($"Failed to parse enum bitpos '{bitposRaw}'", xml);
				}
				value = (long)(1ul << (int)u64);
			}
			else if (xml.Attribute("offset") is { Value: { } offsetRaw }) {
				if (!Int64.TryParse(offsetRaw, out var offset) || offset < 0) {
					throw Error($"Failed to parse enum offset '{offsetRaw}'", xml);
				}

				// Get the extension number as a attribute or a <extension ...> grandparent
				var extNumRaw =
					xml.Attribute("extnumber")?.Value ??
					xml.Parent!.Parent!.Attribute("number")?.Value ??
					throw Error($"Could not find ext number for '{name}'", xml);
				if (!Int64.TryParse(extNumRaw, out var extNum) || extNum <= 0) {
					throw Error($"Failed to parse enum extension number '{extNumRaw}'", xml);
				}

				value = 1_000_000_000 + ((extNum - 1) * 1000) + offset;
			}
			else {
				throw Error($"Failed to extract enum value '{name}'", xml);
			}

			// Flip the value if needed
			if (xml.Attribute("dir")?.Value == "-") {
				value = -value;
			}

			return new(name, outName, value, null);
		}
	}


	// Parses the function pointers
	private static void ParseFunctionPointers(VulkanApi api, XNode root)
	{
		Log.Info("Parsing function pointers");

		// Read the funcpointer types
		var fnptrElemList = root.XPathSelectElements("types/type[@category='funcpointer']");
		foreach (var fnptrElem in fnptrElemList) {
			// Extract name
			var name = fnptrElem.Element("name")?.Value ?? throw Error("Funcpointer without name", fnptrElem);
			if (api.DisabledFeatures.Contains(name)) {
				Log.Verbose($"   Skipping ignored funcpointer '{name}'");
				continue;
			}

			// Extract info
			var subtext = GetSubtext(fnptrElem);
			var ret = _FuncPointerRetRegex.Match(subtext).Groups[1].Value;
			var args = _FuncPointerArgRegex.Matches(subtext).Select(static m => new ApiType.FuncPointer.Arg(
				m.Groups[1].Value // Function pointers are rare so just hardcode replacements here for now
					.Replace(" ", "")
					.Replace("int32_t", "int")
					.Replace("size_t", "nuint")
					.Replace("uint64_t", "ulong")
					.Replace("FlagBits", "Flags"), m.Groups[2].Value));

			// Create the function
			api.FuncPointers.Add(name, new(name, ret.Replace(" ", ""), args.ToArray()));
			Log.Verbose($"   Found funcpointer '{name}' (ret='{ret}', {api.FuncPointers[name].Args.Count} args)");
		}

		// Remove function pointers from the unassigned feature sets
		foreach (var fnPtr in api.FuncPointers.Keys) {
			api.FeatureTypes.Remove(fnPtr);
		}

		Log.Info($"Found {api.FuncPointers.Count} function pointers");
	}


	// Parses the handle types
	private static void ParseHandles(VulkanApi api, XNode root, CancellationToken token)
	{
		Log.Info("Parsing handles");

		var skipped = 0;
		var objTypeEnum = api.Enums["VkObjectType"];
		var parentMap = new Dictionary<ApiType.Handle, string?>();

		// Read the normal handle types
		var handleElemList = root.XPathSelectElements("types/type[@category='handle']");
		foreach (var handleElem in handleElemList) {
			// Extract name
			var name = handleElem.Element("name")?.Value ?? handleElem.Attribute("name")?.Value ??
				throw Error("Handle with missing name", handleElem);
			if (api.DisabledFeatures.Contains(name)) {
				Log.Verbose($"   Skipping disabled handle '{name}'");
				skipped += 1;
				continue;
			}

			// Extract info
			var vendor = VulkanApi.GetVendorForName(name);
			var aliasName = handleElem.Attribute("alias")?.Value;
			var typeEnum = handleElem.Attribute("objtypeenum")?.Value;
			if (aliasName is null && typeEnum is null) {
				throw Error($"Handle '{name}' has null type and alias", handleElem);
			}

			// If alias, lookup
			if (aliasName is not null) {
				if (!api.Handles.TryGetValue(aliasName, out var alias)) {
					throw Error($"Unknown handle alias '{aliasName}' for '{name}'", handleElem);
				}
				api.Handles.Add(name, new(name, alias.ObjectType) { Vendor = vendor });
				parentMap.Add(api.Handles[name], parentMap[alias]);
				Log.Verbose($"   Found handle '{name}' = '{aliasName}'");
			}
			else {
				var parent = handleElem.Attribute("parent")?.Value;
				if (parent is null && name != "VkInstance") {
					throw Error($"Unexpected null handle parent for non-VkInstance '{name}'", handleElem);
				}
				var objTypeName = VulkanApi.NormalizeEnumValueName(objTypeEnum, typeEnum!);
				if (!objTypeEnum.Fields.TryGetValue(objTypeName, out var objType)) {
					throw Error($"Unknown object type '{objTypeName}' for '{name}'", handleElem);
				}
				api.Handles.Add(name, new(name, objType) { Vendor = vendor });
				parentMap.Add(api.Handles[name], parent);
				Log.Verbose($"   Found handle '{name}' (type = '{objTypeName}')");
			}
		}
		token.ThrowIfCancellationRequested();

		// Resolve the parent types
		foreach (var (handle, parentName) in parentMap) {
			if (parentName is null) continue;
			if (!api.Handles.TryGetValue(parentName, out var parent)) {
				throw Error($"Parent handle '{parentName}' for '{handle.OutputName}' cannot be resolved");
			}
			handle.ParentHandle = parent;
		}

		// Remove handles from the unassigned feature sets
		foreach (var handle in api.Handles.Keys) {
			api.FeatureTypes.Remove(handle);
		}

		Log.Info($"Found {api.Handles.Count} handles (skipped {skipped} disabled types)");
	}


	// Parses the struct types
	private static void ParseStructs(VulkanApi api, XNode root, CancellationToken token)
	{
		Log.Info("Parsing structs");

		var skipped = 0;
		var structTypeEnum = api.Enums["VkStructureType"];

		// Read the struct types
		var structElemList = root.XPathSelectElements("types/type[@category='struct' or @category='union']");
		foreach (var structElem in structElemList) {
			// Get the name
			var name = structElem.Attribute("name")?.Value ?? throw Error("Missing struct name", structElem);
			if (api.DisabledFeatures.Contains(name)) {
				api.FeatureTypes.Remove(name);
				Log.Verbose($"   Skipping disabled struct '{name}'");
				skipped += 1;
				continue;
			}
			var vendor = VulkanApi.GetVendorForName(name);

			// Structs can be aliased
			if (structElem.Attribute("alias") is { } aliasElem) {
				if (!api.Structs.TryGetValue(aliasElem.Value, out var alias)) {
					throw Error($"Unknown struct alias target '{aliasElem.Value}' for '{name}'", structElem);
				}
				api.Structs.Add(name, new(name, alias) { Vendor = vendor });
				Log.Verbose($"   Found struct alias '{name}' -> '{aliasElem.Value}'");
			}
			else {
				var isUnion = structElem.Attribute("category")!.Value == "union";
				var isReturnedOnly = structElem.Attribute("returnedonly")?.Value == "true";
				var @struct = new ApiType.Struct(name, isUnion, isReturnedOnly) { Vendor = vendor };

				// Loop over fields
				var fieldElemList = structElem.Elements("member");
				foreach (var fieldElem in fieldElemList) {
					// Extract name and type
					var fieldName = fieldElem.Element("name")?.Value ?? throw Error("Missing field name", fieldElem);
					var typeName = fieldElem.Element("type")?.Value ?? throw Error("Missing field type", fieldElem);

					// Extract extra type info
					var fullField = GetSubtext(fieldElem).Trim();
					var ptrDepth = (uint)fullField.Count(static ch => ch == '*');
					uint arraySize = 1;
					if (fullField.Contains('[')) {
						var matches = _FieldArrayRegex.Matches(fullField);
						for (var mi = 0; mi < matches.Count; ++mi) {
							var matchText = matches[mi].Groups[1].Value;
							if (UInt32.TryParse(matchText, out var matchSize)) {
								arraySize *= matchSize;
							}
							else if (api.Constants.TryGetValue(matchText, out var constant)) {
								if (!UInt32.TryParse(constant.Value[2..], NumberStyles.HexNumber, null,
										out var constantValue)) {
									throw Error($"Invalid constant '{matchText}' = {constant.Value} for array size",
										fieldElem);
								}
								arraySize *= constantValue;
							}
							else {
								throw Error($"Unable to parse field array size '{matchText}'", fieldElem);
							}
						}
					}

					// Lookup type, and handle unresolved types
					if (api.TryGetType(typeName, out var fieldType)) {
						if (ptrDepth > 0) fieldType = new ApiType.Pointer(fieldType, ptrDepth);
						if (arraySize > 1) fieldType = new ApiType.Array(fieldType, arraySize);
					}
					else {
						fieldType = new ApiType.Unresolved(typeName, ptrDepth, arraySize);
					}

					// Extract value where needed
					if (fieldName == "sType" && typeName == "VkStructureType" && @struct.Fields.Count == 0) {
						var typeValue = fieldElem.Attribute("values")?.Value ??
							throw Error($"Missing sType for '{name}'", fieldElem);
						var enumValueName = VulkanApi.NormalizeEnumValueName(structTypeEnum, typeValue);
						if (!structTypeEnum.Fields.TryGetValue(enumValueName, out var enumValue)) {
							throw Error($"Unknown sType value '{enumValueName}'", fieldElem);
						}
						@struct.StructureType = enumValue;
					}

					// Look for bitfield
					uint? bitfield = null;
					if (Char.IsDigit(fullField[^1]) && !Char.IsDigit(fieldName[^1])) {
						var match = _FieldBitfieldRegex.Match(fullField);
						if (!match.Success || !UInt32.TryParse(match.Groups[1].Value, out var bfSize)) {
							throw Error($"Failed to parse bitfield from '{fullField}'", fieldElem);
						}
						bitfield = bfSize;
					}

					// Add the field
					@struct.AddField(fieldName, fieldType, bitfield);
				}

				// Build the struct
				api.Structs.Add(name, @struct);
				Log.Verbose($"   Found struct '{name}' ({@struct.Fields.Count} fields)");
			}
		}
		token.ThrowIfCancellationRequested();

		// Build the struct types
		Log.Info($"Found {api.Structs.Count} structs (skipped {skipped} disabled types)");
		Log.Info("Building structs");
		foreach (var @struct in api.Structs.Values) {
			@struct.Build(api);
			if (@struct.TypeSize == 0) {
				throw new SpecError($"Failed to build struct '{@struct.OutputName}' (zero size)");
			}
		}

		// Remove structs from the unassigned feature sets
		foreach (var @struct in api.Structs.Keys) {
			api.FeatureTypes.Remove(@struct);
		}

		Log.Info($"Built {api.Structs.Count} structs");
	}


	// Parses the commands/functions
	private static void ParseFunctions(VulkanApi api, XNode root, CancellationToken token)
	{
		Log.Info("Parsing functions");

		var skipped = 0;

		// Enumerate the list of functions
		var argList = new List<(ApiType, string)>();
		var cmdElemList = root.XPathSelectElements("commands/command");
		foreach (var cmdElem in cmdElemList) {
			argList.Clear();

			// Extract and check name
			var name = cmdElem.Attribute("name")?.Value ?? cmdElem.Element("proto")?.Element("name")?.Value ??
				throw Error("Command without name", cmdElem);
			if (api.DisabledFeatures.Contains(name)) {
				Log.Verbose($"   Skipping ignored command '{name}'");
				skipped += 1;
				continue;
			}
			var vendor = VulkanApi.GetVendorForName(name);

			// Handle aliases
			if (cmdElem.Attribute("alias") is { Value: var aliasName }) {
				if (!api.Functions.TryGetValue(aliasName, out var alias)) {
					throw Error($"Cannot find alias '{aliasName}' for function '{name}'", cmdElem);
				}
				api.Functions.Add(name, alias with { Name = name, Vendor = vendor });
				Log.Verbose($"   Found function alias '{name}' -> '{aliasName}'");
				continue;
			}

			// Extract return type
			var (retType, _) = extractElement(api,
				cmdElem.Element("proto") ?? throw Error("Command without proto", cmdElem));

			// Extract args
			foreach (var paramElem in cmdElem.Elements("param")) {
				var (argType, argName) = extractElement(api, paramElem);
				argList.Add((argType, argName));
			}

			api.Functions.Add(name, new(name, retType, argList.ToArray()) { Vendor = vendor });
			Log.Verbose($"   Found function '{name}' (ret={retType.OutputName}, {argList.Count} args)");
		}
		token.ThrowIfCancellationRequested();

		// Function sanity checks
		foreach (var func in api.Functions.Values) {
			if (!func.IsGlobal && func.Args[0].Type is not ApiType.Handle) { // Important assumption
				throw Error($"Function '{func.Name}' is non-global and non-dispatchable");
			}
			switch ((func.IsGlobal ? 1 : 0) + (func.IsInstance ? 1 : 0) + (func.IsDevice ? 1 : 0))
			{
			case 0:     throw Error($"Function '{func.Name}' is not one of the function kinds");
			case not 1: throw Error($"Function '{func.Name}' is more than one function kind");
			}
		}

		// Remove functions from the unassigned feature sets
		foreach (var func in api.Functions.Keys) {
			api.FeatureCommands.Remove(func);
		}

		Log.Info($"Found {api.Functions.Count} functions (skipped {skipped} disabled functions)");
		return;

		// Extracts type and name elements from a command element
		static (ApiType Type, string Name) extractElement(VulkanApi api, XElement element)
		{
			// Extract
			var nameRaw = element.Element("name")?.Value ?? throw Error("Command element without name", element);
			var typeRaw = element.Element("type")?.Value ?? throw Error("Command element without type", element);
			element.Element("comment")?.Remove();
			var subtext = GetSubtext(element);
			var ptrDepth = (uint)subtext.Count(static ch => ch == '*');

			// Fix name, only a few cases here so hard-coding is okay
			nameRaw = nameRaw.Replace("event", "@event").Replace("object", "@object");

			// Lookup
			typeRaw = typeRaw.Replace("FlagBits", "Flags");
			if (!api.TryGetType(typeRaw, out var type)) {
				throw Error($"Command element has unknown type '{typeRaw}'", element);
			}
			if (ptrDepth > 0) {
				type = new ApiType.Pointer(type, ptrDepth);
			}

			return (type, nameRaw);
		}
	}
}
