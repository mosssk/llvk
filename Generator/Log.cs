﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Runtime.CompilerServices;

namespace LLVK;


// Basic thread-safe logger that supports log levels and colors
internal static class Log
{
	// Default color
	private static readonly ConsoleColor _DefaultColor = Console.ForegroundColor;
	// Console lock
	private static readonly object _ConsoleLock = new();
	// Verbose enabled flag
	public static bool IsVerbose { get; private set; }


	// Enable verbose logging
	public static void SetVerbose(bool flag) => IsVerbose = flag;


	// Verbose output
	public static void Verbose(string msg)
	{
		if (!IsVerbose) return;
		lock (_ConsoleLock) {
			Console.ForegroundColor = ConsoleColor.DarkGray;
			Console.WriteLine(msg);
			Console.ForegroundColor = _DefaultColor;
		}
	}

	// Verbose output
	public static void Verbose(ref VerboseLogStringHandler msg)
	{
		if (!IsVerbose) return;
		lock (_ConsoleLock) {
			Console.ForegroundColor = ConsoleColor.DarkGray;
			Console.WriteLine(msg.ToStringAndClear());
			Console.ForegroundColor = _DefaultColor;
		}
	}

	// Normal info output
	public static void Info(string msg)
	{
		lock (_ConsoleLock) {
			Console.WriteLine(msg);
		}
	}

	// Warning output
	public static void Warn(string msg)
	{
		lock (_ConsoleLock) {
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine(msg);
			Console.ForegroundColor = _DefaultColor;
		}
	}

	// Error output
	public static void Error(string msg)
	{
		lock (_ConsoleLock) {
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Error.WriteLine(msg);
			Console.ForegroundColor = _DefaultColor;
		}
	}


	// String interpolator for verbose logging
	[InterpolatedStringHandler]
	public ref struct VerboseLogStringHandler
	{
		private DefaultInterpolatedStringHandler _handler;

		public VerboseLogStringHandler(int literalLength, int formattedCount, out bool enabled)
		{
			_handler = IsVerbose ? new(literalLength, formattedCount) : new(0, 0);
			enabled = IsVerbose;
		}

		public override string ToString() => _handler.ToString();
		public string ToStringAndClear() => _handler.ToStringAndClear();

		public void AppendLiteral(string s) => _handler.AppendLiteral(s);
		public void AppendFormatted<T>(T t, int alignment = 0, string? format = null) =>
			_handler.AppendFormatted(t, alignment, format);
	}
}
