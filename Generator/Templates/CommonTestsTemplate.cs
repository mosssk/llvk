﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace LLVK.Templates;


// Template for generating top-level common functionality tests
internal sealed class CommonTestsTemplate : TestsTemplateBase
{
	protected override void GenerateBody()
	{
		// Tests for the generated versions
		using (OpenTestCase("ApiVersions", true)) {
			GenAssert_EqualTo("VK_HEADER_VERSION", Api.SpecVersion.Build);
			GenAssert_EqualTo("VK_HEADER_VERSION_COMPLETE",
				$"VK_MAKE_API_VERSION(0, {Api.SpecVersion.Major}, {Api.SpecVersion.Minor}, {Api.SpecVersion.Build})");
		}

		// Tests for the assembly versions
		using (OpenTestCase("AssemblyVersions")) {
			var genVer = typeof(SpecGenerator).Assembly.GetName().Version!.Major;
			Source.AppendLine("\tvar specVer = typeof(LLVK.Vulkan).Assembly.GetName().Version!;");
			Source.AppendLine("\tvar testVer = typeof(Core_Tests).Assembly.GetName().Version!;");
			GenAssert_EqualTo("specVer.Major", Api.SpecVersion.Major);
			GenAssert_EqualTo("specVer.Minor", Api.SpecVersion.Minor);
			GenAssert_EqualTo("specVer.Build", Api.SpecVersion.Build);
			GenAssert_EqualTo("specVer.Revision", genVer);
			GenAssert_EqualTo("testVer.Major", Api.SpecVersion.Major);
			GenAssert_EqualTo("testVer.Minor", Api.SpecVersion.Minor);
			GenAssert_EqualTo("testVer.Build", Api.SpecVersion.Build);
			GenAssert_EqualTo("testVer.Revision", genVer);
		}

		// Tests for API constants
		using (OpenTestCase("ApiConstantValues")) {
			foreach (var @const in Api.Constants.Values) {
				var expected = @const.SpecValue.ToLowerInvariant().TrimStart('(').TrimEnd(')').TrimEnd('f')
					.Replace("ull", "ul");
				GenAssert_EqualTo(@const.Name, expected);
			}
		}

		// Tests for structure chains
		using (OpenTestCase("StructureChains")) {
			Source
				.AppendLine("\tVulkanStructureChain<VkApplicationInfo, VkDeviceQueueCreateInfo> chain = new();")
				.AppendLine("\tAssert.That(chain.Value0.SType, Is.EqualTo(VkStructureType.ApplicationInfo));")
				.AppendLine("\tAssert.That(chain.Value1.SType, Is.EqualTo(VkStructureType.DeviceQueueCreateInfo));")
				.AppendLine("\tchain.Value0.ApplicationVersion = 10;")
				.AppendLine("\tchain.Value1.QueueFamilyIndex = 20;")
				.AppendLine("\tchain.Invoke(ptr => {")
				.AppendLine("\t\tAssert.That(ptr->SType, Is.EqualTo(VkStructureType.ApplicationInfo));")
				.AppendLine("\t\tAssert.That(ptr->ApplicationVersion, Is.EqualTo(10));")
				.AppendLine("\t\tvar next = (VkDeviceQueueCreateInfo*)ptr->PNext;")
				.AppendLine("\t\tAssert.That(next->SType, Is.EqualTo(VkStructureType.DeviceQueueCreateInfo));")
				.AppendLine("\t\tAssert.That(next->QueueFamilyIndex, Is.EqualTo(20));")
				.AppendLine("\t\tptr->ApplicationVersion = 100;")
				.AppendLine("\t\tnext->QueueFamilyIndex = 200;")
				.AppendLine("\t});")
				.AppendLine("\tAssert.That(chain.Value0.ApplicationVersion, Is.EqualTo(100));")
				.AppendLine("\tAssert.That(chain.Value1.QueueFamilyIndex, Is.EqualTo(200));");
		}
	}
}
