﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace LLVK.Templates;


// Base type for testing templates
internal abstract class TestsTemplateBase : TemplateBase
{
	protected sealed override bool IsTests => true;
	protected override string ClassName => Vendor?.Tag ?? "Core";

	protected override void GenerateUsings()
	{
		Source.AppendLine("using static LLVK.Vulkan;");
		if (Vendor is not null) {
			Source.Append("using static LLVK.Vulkan.").Append(Vendor.Tag).AppendLine(";");
		}
	}

	protected override void GenerateHeader()
	{
		Source.AppendLine("#pragma warning disable CS0618 // Type or member is obsolete"); // Some types are obsolete
		Source.AppendLine("#pragma warning disable CS1718 // Comparison to self"); // Self-comparisons are used
	}


	// Generate EqualTo Assert
	protected void GenAssert_EqualTo<T, U>(T actual, U expected) =>
		Source.Append("\tAssert.That(").Append(actual).Append(", Is.EqualTo(").Append(expected).AppendLine("));");


	// Starts a test method and returns an auto-closing monitor
	protected TestCaseGen OpenTestCase(string testName, bool allowActualConstants = false)
	{
		Source.AppendLine("[Test]");
		if (allowActualConstants) {
			Source.AppendLine("[SuppressMessage(\"Assertion\", \"NUnit2007\")]"); // Constant value as actual arg
		}
		Source.Append("public void Test_").Append(testName).AppendLine("()");
		Source.AppendLine("{");
		return new(this);
	}

	// Auto-closing test case monitor
	protected readonly ref struct TestCaseGen
	{
		private readonly TestsTemplateBase _template;
		public TestCaseGen(TestsTemplateBase template) => _template = template;
		public void Dispose() => _template.Source.AppendLine("}").AppendLine();
	}
}
