﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace LLVK.Templates;


// Template for generating tests for API helpers
internal sealed class HelperTestsTemplate : TestsTemplateBase
{
	protected override string ClassName => "VulkanHelper";

	protected override void GenerateBody()
	{
		// VkResult Classification
		using (OpenTestCase("VkResult_Classification")) {
			Source.AppendLine("\tforeach (var value in Enum.GetValues<VkResult>()) {");
			Source.AppendLine("\t\tAssert.That(value.IsError(), Is.EqualTo((int)value < 0));");
			Source.AppendLine("\t\tAssert.That(value.IsSuccess(), Is.EqualTo(value == VkResult.Success));");
			Source.AppendLine("\t\tAssert.That(value.IsIndeterminate(), Is.EqualTo((int)value > 0));");
			Source.AppendLine("\t}");
		}

		// VkResult throw
		using (OpenTestCase("VkResult_ThrowIfError")) {
			Source.AppendLine("\tforeach (var value in Enum.GetValues<VkResult>()) {");
			Source.AppendLine("\t\tif (value.IsError()) Assert.Throws<VulkanHelper.ResultException>(() => value.ThrowIfError());");
			Source.AppendLine("\t\telse Assert.DoesNotThrow(() => value.ThrowIfError());");
			Source.AppendLine("\t}");
		}
	}
}
