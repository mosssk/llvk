﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LLVK.Templates;


// Template for generating core and vendor enums and bitmasks
internal sealed class VendorTemplate : TemplateBase
{
	protected override void GenerateHeader() =>
		Source.AppendLine("// ReSharper disable RedundantNameQualifier ArrangeStaticMemberQualifier");

	protected override void GenerateBody()
	{
		// Generate extension constants
		foreach (var ext in Api.Extensions.Values.Where(e => ReferenceEquals(e.Vendor, Vendor))) {
			Source.Append("public const string ")
				.Append(ext.ExtNameName)
				.Append(" = \"")
				.Append(ext.ExtNameValue)
				.AppendLine("\";");
			Source.Append("public const uint ")
				.Append(ext.ExtVersionName)
				.Append(" = ")
				.Append(ext.ExtVersionValue)
				.AppendLine(";");
			Source.AppendLine();
		}

		// Generate enums
		foreach (var @enum in Api.Enums.Values.Where(e => ReferenceEquals(e.Vendor, Vendor))) {
			// Write header
			if (@enum.IsBitmask) Source.AppendLine("[Flags]");
			Source.Append("public enum ")
				.Append(@enum.OutputName)
				.Append(" : ")
				.AppendLine((@enum.IsWide, @enum.IsBitmask) switch {
					(false, false) => "int", (false, true) => "uint",
					(true, false)  => "long", (true, true) => "ulong"
				});
			Source.AppendLine("{");

			// Write values
			foreach (var field in @enum.Fields.Values) {
				Source.Append("\t")
					.Append(field.OutputName)
					.Append(" = ")
					.Append(field.Alias ?? (@enum.IsWide, @enum.IsBitmask) switch {
						(true, true)  => $"0x{field.Value:X16}",
						(false, true) => $"0x{field.Value:X8}",
						(_, false)    => field.Value.ToString()
					})
					.AppendLine(",");
			}

			// Write close
			Source.AppendLine("}");
			Source.AppendLine();
		}

		// Generate handles
		foreach (var handle in Api.Handles.Values.Where(e => ReferenceEquals(e.Vendor, Vendor))) {
			// Write header
			Source.AppendLine("[StructLayout(LayoutKind.Sequential, Size = 8)]");
			Source.Append("public readonly partial struct ")
				.Append(handle.OutputName)
				.Append(" : IEquatable<")
				.Append(handle.OutputName)
				.AppendLine(">, IVulkanHandle");
			Source.AppendLine("{");

			// Write fields
			Source.Append("\tpublic VkObjectType ObjectType => VkObjectType.")
				.Append(handle.ObjectType.OutputName)
				.AppendLine(";");
			Source.AppendLine("\tpublic nuint Handle => _handle;");
			Source.AppendLine("\tprivate readonly nuint _handle;");
			Source.AppendLine();

			// Write methods
			Source.Append("\tpublic override string ToString() => $\"")
				.Append(handle.OutputName)
				.AppendLine("[0x{Handle:X16}]\";");
			Source.AppendLine("\tpublic override int GetHashCode() => Handle.GetHashCode();");
			Source.Append("\tpublic override bool Equals([NotNullWhen(true)] object? o) => o is ")
				.Append(handle.OutputName)
				.AppendLine(" h && h.Handle == Handle;");
			Source.Append("\tpublic bool Equals(")
				.Append(handle.OutputName)
				.AppendLine(" o) => o.Handle == Handle;");
			Source.AppendLine();

			// Write operators
			Source.Append("\tpublic static bool operator == (")
				.Append(handle.OutputName)
				.Append(" l, ")
				.Append(handle.OutputName)
				.AppendLine(" r) => l._handle == r._handle;");
			Source.Append("\tpublic static bool operator != (")
				.Append(handle.OutputName)
				.Append(" l, ")
				.Append(handle.OutputName)
				.AppendLine(" r) => l._handle != r._handle;");
			Source.Append("\tpublic static implicit operator bool (")
				.Append(handle.OutputName)
				.AppendLine(" r) => r._handle != 0;");

			// Write close
			Source.AppendLine("}");
			Source.AppendLine();
		}

		// Generate structs
		var arrayTypes = new HashSet<string>(); // Prevent double-generation of array types
		foreach (var @struct in Api.Structs.Values.Where(e => ReferenceEquals(e.Vendor, Vendor))) {
			arrayTypes.Clear();

			// Write header
			Source.Append("[StructLayout(LayoutKind.Explicit, Size = ")
				.Append(@struct.TypeSize)
				.AppendLine(")]");
			Source.Append("public partial struct ").Append(@struct.OutputName)
				.AppendLine(@struct.IsChainable ? " : IVulkanChainableStruct" : "");
			Source.AppendLine("{");

			// Write fields
			var hasArrayField = false;
			var hasSTypeField = false;
			foreach (var field in @struct.Fields.Values.OrderBy(static f => f.Order)) {
				Source.Append("\t[FieldOffset(")
					.Append(field.Offset)
					.Append(")] ")
					.Append(field.Name[0] == '_' ? "private " : "public ")
					.Append(field.Type.QualifiedName)
					.Append(' ')
					.Append(field.Name)
					.Append(" = ");
				if (field is { Name: "_sType", Order: 0 } && @struct.StructureType is not null) {
					Source.Append("VkStructureType.").Append(@struct.StructureType!.OutputName);
					hasSTypeField = true;
				}
				else {
					Source.Append("default");
				}
				Source.AppendLine(";");
				hasArrayField |= field.Type is ApiType.Array;
			}

			// Write bitfields
			if (@struct.Bitfields.Count > 0) {
				Source.AppendLine();
				foreach (var bf in @struct.Bitfields.Values) {
					var mask = makeMaskString(bf.Offset, bf.Size);
					Source
						.Append("\tpublic ")
						.Append(bf.Type.QualifiedName)
						.Append(' ')
						.Append(bf.Name)
						.AppendLine(" {");
					Source
						.Append("\t\treadonly get => (")
						.Append(bf.Type.QualifiedName)
						.Append(")((_bitfield")
						.Append(bf.Number)
						.Append(" & ")
						.Append(mask)
						.Append(") >> ")
						.Append(bf.Offset)
						.AppendLine(");");
					Source
						.Append("\t\tset => _bitfield")
						.Append(bf.Number)
						.Append(" = (uint)((_bitfield")
						.Append(bf.Number)
						.Append(" & ~")
						.Append(mask)
						.Append(") | (((uint)value << ")
						.Append(bf.Offset)
						.Append(") & ")
						.Append(mask)
						.AppendLine("));");
					Source.AppendLine("\t}");
				}
			}

			// Write chainable fields
			if (@struct.IsChainable) {
				Source.AppendLine();
				Source.AppendLine("\tpublic VkStructureType SType { readonly get => _sType; set => _sType = value; }");
				Source.AppendLine("\tpublic void* PNext { readonly get => _pNext; set => _pNext = value; }");
			}

			// Write Ctor
			Source.AppendLine();
			if (@struct.IsReturnedOnly && !hasSTypeField) {
				Source.Append("\t[Obsolete(\"Type ").Append(@struct.OutputName)
					.AppendLine(" is marked as return-only.\")]");
			}
			Source.Append("\tpublic ")
				.Append(@struct.OutputName)
				.AppendLine("() { }");

			// Write field ctor if needed
			if (@struct.GenCtor) {
				Source.AppendLine();
				Source.Append("\tpublic ").Append(@struct.OutputName).AppendLine("(");
				foreach (var field in @struct.Fields.Values.OrderBy(static f => f.Order).SkipLast(1)) {
					Source.Append("\t\t").Append(field.Type.QualifiedName).Append(' ').Append(field.SpecName)
						.AppendLine(",");
				}
				var last = @struct.Fields.Values.OrderBy(static f => f.Order).Last();
				Source.Append("\t\t").Append(last.Type.QualifiedName).Append(' ').AppendLine(last.SpecName);
				Source.AppendLine("\t) {");
				foreach (var field in @struct.Fields.Values.OrderBy(static f => f.Order)) {
					Source.Append("\t\t").Append(field.Name).Append(" = ").Append(field.SpecName).AppendLine(";");
				}
				Source.AppendLine("\t}");
			}

			// Write array types if needed
			if (hasArrayField) {
				var arrFields = @struct.Fields.Values.Where(static f => f.Type is ApiType.Array);
				foreach (var arrField in arrFields) {
					var arrType = (ApiType.Array)arrField.Type;
					if (!arrayTypes.Add(arrType.OutputName)) continue; // Already generated

					// Generate
					Source.AppendLine();
					Source.AppendLine("\t[StructLayout(LayoutKind.Explicit)]");
					Source.Append("\tpublic struct ")
						.AppendLine(arrType.OutputName);
					Source.AppendLine("\t{");
					Source.Append("\t\tpublic const uint COUNT = ")
						.Append(arrType.ArraySize)
						.Append(", ELEM_SIZE = ")
						.Append(arrType.BaseType.TypeSize)
						.AppendLine(";");
					Source.Append("\t\t[FieldOffset(0)] private fixed byte _data[")
						.Append(arrType.TypeSize)
						.AppendLine("];");
					Source.Append("\t\tpublic ref ")
						.Append(arrType.BaseType.QualifiedName)
						.AppendLine(" this [int index] { get { ")
						.AppendLine("\t\t\tif (index is < 0 or >= (int)COUNT) throw new ArgumentOutOfRangeException(nameof(index));")
						.Append("\t\t\treturn ref Unsafe.As<byte, ")
						.Append(arrType.BaseType.QualifiedName)
						.AppendLine(">(ref Unsafe.Add(ref _data[0], (uint)index * ELEM_SIZE));")
						.AppendLine("\t\t} }");
					Source.Append("\t\tpublic ref ")
						.Append(arrType.BaseType.QualifiedName)
						.AppendLine(" this [uint index] => ref this[(int)index];");
					Source.AppendLine("\t}");
				}
			}

			// Write close
			Source.AppendLine("}");
			Source.AppendLine();
		}
		Source.AppendLine();
		Source.AppendLine();


		// Function stubs
		foreach (var func in Api.Functions.Values.Where(f => ReferenceEquals(f.Vendor, Vendor))) {
			Source.AppendLine("[MethodImpl(MethodImplOptions.AggressiveInlining)]");
			Source.Append("public static ")
				.Append(func.ReturnType.QualifiedName)
				.Append(' ')
				.Append(func.Name)
				.Append('(')
				.Append(String.Join(", ", func.Args.Select(static a => $"{a.Type.QualifiedName} {a.Name}")))
				.AppendLine(") =>");
			Source.Append("\tF_.")
				.Append(func.Name)
				.Append("_(")
				.Append(String.Join(", ", func.Args.Select(static a => a.Name)))
				.AppendLine(");");
			Source.AppendLine();
		}
		Source.AppendLine();


		// Functions
		Source.AppendLine("#pragma warning disable CS0649 // Unassigned fields");
		Source.AppendLine("internal static class F_");
		Source.AppendLine("{");

		// Generate function pointers
		foreach (var func in Api.Functions.Values.Where(f => ReferenceEquals(f.Vendor, Vendor))) {
			Source.Append("\tpublic static delegate* unmanaged<")
				.Append(String.Join(", ",
					func.Args.Select(static a => a.Type.QualifiedName).Append(func.ReturnType.QualifiedName)))
				.Append("> ")
				.Append(func.Name)
				.AppendLine("_;");
		}
		Source.AppendLine();

		// Instance function loading
		Source.AppendLine("\t[SkipLocalsInit]");
		Source.AppendLine("\tpublic static void LoadInstanceFunctions(VkInstance handle)");
		Source.AppendLine("\t{");
		Source.AppendLine("\t\tvar nameBuffer = stackalloc byte[256];"); // 256 is large enough for all functions
		foreach (var func in Api.Functions.Values.Where(f => f.IsInstance && ReferenceEquals(f.Vendor, Vendor))) {
			GenerateLoad(Source, func, "Vulkan.F_.vkGetInstanceProcAddr_(handle");
		}
		Source.AppendLine("\t}");
		Source.AppendLine();

		// Device function loading
		Source.AppendLine("\t[SkipLocalsInit]");
		Source.AppendLine("\tpublic static void LoadDeviceFunctions(VkDevice handle)");
		Source.AppendLine("\t{");
		Source.AppendLine("\t\tvar nameBuffer = stackalloc byte[256];"); // 256 is large enough for all functions
		foreach (var func in Api.Functions.Values.Where(f => f.IsDevice && ReferenceEquals(f.Vendor, Vendor))) {
			GenerateLoad(Source, func, "Vulkan.F_.vkGetDeviceProcAddr_(handle");
		}
		Source.AppendLine("\t}");

		// Close functions
		Source.AppendLine("}");

		return;

		static string makeMaskString(uint offset, uint count)
		{
			var mask = (0xFFFFFFFFu >> (int)(32 - count)) << (int)offset;
			return $"0x{mask:X8}";
		}
	}


	public static void GenerateLoad(StringBuilder source, VulkanApi.Function func, string loadPrefix) =>
		source
			.Append("\t\tVulkanLoader.PopulateNameBuffer(\"").Append(func.Name).AppendLine("\", nameBuffer, 256);")
			.Append("\t\tF_.").Append(func.Name).AppendLine("_ = ")
			.Append("\t\t\t(delegate* unmanaged<")
			.Append(String.Join(", ",
				func.Args.Select(static a => a.Type.QualifiedName).Append(func.ReturnType.QualifiedName)))
			.AppendLine(">)")
			.Append("\t\t\t").Append(loadPrefix).AppendLine(", nameBuffer);");
}
