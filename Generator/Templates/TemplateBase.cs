﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LLVK.Templates;


// Base type for source generation templates
internal abstract class TemplateBase
{
	#region Fields
	// The source builder for the template
	protected readonly StringBuilder Source = new(16 * 1024 * 1024); // Start big
	// If the template is for generating tests
	protected virtual bool IsTests => false;
	// The top-level generated class names
	protected virtual string ClassName => "Vulkan";

	// If the template generates C++ (false is C#)
	protected virtual bool IsCpp => false;

	// The current API instance
	protected VulkanApi Api { get; private set; } = null!;
	// The current vendor, if any
	protected VulkanApi.Vendor? Vendor { get; private set; }

	// The cancellation token
	protected CancellationToken Token { get; private set; } = CancellationToken.None;
	#endregion // Fields


	// Performs the generation for the template and the given vendor
	//    Returns if generation occured (false means the file is effectively empty)
	public bool Generate(VulkanApi api, VulkanApi.Vendor? vendor, CancellationToken token)
	{
		// Setup state
		Api = api;
		Vendor = vendor;
		Token = token;
		Source.Clear();

		// Generate the header
		Source.AppendLine(@$"
/*
 * MIT License - Copyright (c) {DateTime.Now.Year} Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 *
 * This file is source generated from the Vulkan specification. DO NOT EDIT BY HAND.
 */"
		);

		return IsCpp ? generateCpp(api, vendor, token) : generateCSharp(api, vendor, token);
	}

	// C# generation
	private bool generateCSharp(VulkanApi api, VulkanApi.Vendor? vendor, CancellationToken token)
	{
		// Generate the header
		Source.AppendLine();
		Source.AppendLine("using System;");
		Source.AppendLine("using System.Runtime.CompilerServices;");
		Source.AppendLine("using System.Runtime.InteropServices;");
		Source.AppendLine("using System.Diagnostics.CodeAnalysis;");
		GenerateUsings();
		Source.AppendLine();
		Source.AppendLine("// ReSharper disable InconsistentNaming PartialTypeWithSinglePart UnusedType.Global");
		Source.AppendLine("// ReSharper disable UnusedMember.Global EnumUnderlyingTypeIsInt IdentifierTypo StringLiteralTypo");
		Source.AppendLine("// ReSharper disable ConvertToAutoProperty");
		GenerateHeader();
		Source.AppendLine();
		Source.Append("namespace LLVK").Append(IsTests ? ".Tests" : "").AppendLine(";");
		Source.AppendLine();
		Source.AppendLine();

		// Open the classes
		if (IsTests) {
			Source.Append("public unsafe partial class ").Append(ClassName).AppendLine("_Tests {");
		} else {
			Source.Append("public static unsafe partial class ").Append(ClassName).AppendLine(" {");
			if (Vendor is not null) {
				Source.Append("public static unsafe partial class ").Append(Vendor.Tag).AppendLine(" {");
			}
		}
		Source.AppendLine();

		// Generate the body
		var len = Source.Length;
		GenerateBody();
		if (Source.Length == len) return false;

		// Close the classes
		Source.AppendLine();
		Source.AppendLine("}");
		if (!IsTests && vendor is not null) Source.AppendLine("}");

		return true;
	}

	// C++ generation
	private bool generateCpp(VulkanApi api, VulkanApi.Vendor? vendor, CancellationToken token)
	{
		Source.AppendLine();

		// Generate the header
		Source.AppendLine("#include <llvktests.h>").AppendLine();
		GenerateUsings();
		GenerateHeader();
		Source.AppendLine();

		// Generate the body
		var len = Source.Length;
		GenerateBody();
		return Source.Length != len;
	}


	// Saves the generated source to the file
	public async Task WriteToFileAsync(string path, CancellationToken token) =>
		await File.WriteAllTextAsync(path, Source.ToString(), Encoding.UTF8, token);


	// Generates additional usings
	protected virtual void GenerateUsings() { }

	// Generates the header after the usings
	protected virtual void GenerateHeader() { }

	// Generates the main body of the template
	protected abstract void GenerateBody();
}
