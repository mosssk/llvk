﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Linq;

namespace LLVK.Templates;


// Template for generating the loader code for the Vulkan library and functions
internal sealed class LoaderTemplate : TemplateBase
{
	protected override string ClassName => "VulkanLoader";

	protected override void GenerateUsings() => Source.AppendLine("using static LLVK.Vulkan;");

	protected override void GenerateHeader() =>
		Source.AppendLine("// ReSharper disable RedundantNameQualifier ArrangeStaticMemberQualifier");

	protected override void GenerateBody()
	{
		// Library and global function loader
		Source.AppendLine("internal static partial void LoadLibraryAndGlobalFunctions()");
		Source.AppendLine("{");

		// Skip loading if requested
		Source.AppendLine("\tif (IsLoaderDisabled) return;");
		Source.AppendLine();

		// Load the library
		Source
			.AppendLine("\tforeach (var name in EnumerateLibraryNames()) {")
			.AppendLine("\t\tif (!NativeLibrary.TryLoad(name, out var handle)) continue;")
			.AppendLine("\t\tLibraryHandle = handle;")
			.AppendLine("\t\tbreak;")
			.AppendLine("\t}");
		Source
			.AppendLine("\tif (LibraryHandle == IntPtr.Zero) {")
			.AppendLine("\t\tthrow new PlatformNotSupportedException(\"Cannot find the Vulkan loader on the current platform\");")
			.AppendLine("\t}");
		Source.AppendLine("\tAppDomain.CurrentDomain.ProcessExit += (_, _) => NativeLibrary.Free(LibraryHandle);");
		Source.AppendLine();

		// Global functions
		foreach (var func in Api.Functions.Values.Where(static f => f.IsGlobal)) {
			Source
				.Append("\tF_.").Append(func.Name).AppendLine("_ = ")
				.Append("\t\t(delegate* unmanaged<")
					.Append(String.Join(", ",
						func.Args.Select(static a => a.Type.OutputName).Append(func.ReturnType.OutputName)))
					.AppendLine(">)")
				.Append("\t\tNativeLibrary.GetExport(LibraryHandle, \"").Append(func.Name).AppendLine("\");");
		}

		// Close
		Source.AppendLine("}");
		Source.AppendLine();


		// Instance-level function loading
		Source.AppendLine("public static partial void LoadInstanceFunctions(Vulkan.VkInstance handle)");
		Source.AppendLine("{");
		Source.AppendLine("\tif (IsLoaderDisabled) return;");
		Source.AppendLine();
		Source.AppendLine("\tVulkan.F_.LoadInstanceFunctions(handle);");
		Source.AppendLine();
		foreach (var vendor in VulkanApi.KnownVendors.Values) {
			Source.Append('\t').Append(vendor.Tag).AppendLine(".F_.LoadInstanceFunctions(handle);");
		}
		Source.AppendLine("}");
		Source.AppendLine();


		// Device-level function loading
		Source.AppendLine("public static partial void LoadDeviceFunctions(Vulkan.VkDevice handle)");
		Source.AppendLine("{");
		Source.AppendLine("\tif (IsLoaderDisabled) return;");
		Source.AppendLine();
		Source.AppendLine("\tVulkan.F_.LoadDeviceFunctions(handle);");
		Source.AppendLine();
		foreach (var vendor in VulkanApi.KnownVendors.Values) {
			Source.Append('\t').Append(vendor.Tag).AppendLine(".F_.LoadDeviceFunctions(handle);");
		}
		Source.AppendLine("}");
		Source.AppendLine();


		// Function availability checks
		Source.AppendLine("public static partial void* GetFunctionPointer(string funcName) => funcName switch {");
		foreach (var func in Api.Functions.Values) {
			Source.Append("\t\"")
				.Append(func.Name)
				.Append("\" => Vulkan.")
				.Append(func.Vendor is { } v ? v.Tag + "." : "")
				.Append("F_.")
				.Append(func.Name)
				.AppendLine("_,");
		}
		Source.AppendLine("\t_ => throw new ArgumentException($\"Unknown Vulkan API function '{funcName}'\", funcName)");
		Source.AppendLine("};");
	}
}
