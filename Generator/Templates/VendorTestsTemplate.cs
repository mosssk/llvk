﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Linq;

namespace LLVK.Templates;


// Template for generating tests for core and vendor enums and bitmasks
internal sealed class VendorTestsTemplate : TestsTemplateBase
{
	protected override void GenerateBody()
	{
		foreach (var @enum in Api.Enums.Values.Where(e => ReferenceEquals(e.Vendor, Vendor))) {
			using (OpenTestCase(@enum.SpecName)) {
				Source
					.Append("\tvar func = (delegate* unmanaged<long*, uint>)")
					.Append("NativeTestLib.GetFunction(\"get_").Append(@enum.OutputName).AppendLine("_values\");");
				Source.AppendLine("\tif (func == null) Assert.Fail(\"Could not load native function\");");
				Source.Append("\tvar values = stackalloc long[").Append(@enum.Fields.Count).AppendLine("];");
				Source.Append("\tAssert.That(func(values), Is.EqualTo(").Append(@enum.TypeSize).AppendLine("));");
				uint fi = 0;
				foreach (var field in @enum.Fields.Values.OrderBy(static f => f.Value)) {
					Source
						.Append("\tAssert.That(values[").Append(fi++).Append("], Is.EqualTo(").Append(field.Value)
						.Append(")); // ").AppendLine(field.OutputName);
				}
			}
		}

		foreach (var @struct in Api.Structs.Values.Where(s => ReferenceEquals(s.Vendor, Vendor))) {
			using (OpenTestCase(@struct.SpecName)) {
				Source
					.Append("\tvar func = (delegate* unmanaged<uint*, uint>)")
					.Append("NativeTestLib.GetFunction(\"get_").Append(@struct.OutputName).AppendLine("_info\");");
				Source.AppendLine("\tif (func == null) Assert.Fail(\"Could not load native function\");");
				Source.Append("\tvar values = stackalloc uint[").Append(@struct.Fields.Count).AppendLine("];");
				Source
					.Append("\tAssert.That(sizeof(").Append(@struct.OutputName).Append("), Is.EqualTo(")
					.Append(@struct.TypeSize).AppendLine("));");
				Source.Append("\tAssert.That(func(values), Is.EqualTo(").Append(@struct.TypeSize).AppendLine("));");
				uint fi = 0;
				foreach (var field in @struct.Fields.Values.OrderBy(static f => f.Offset)) {
					if (field.Name.StartsWith("_bit")) continue; // Skip bitfields
					Source
						.Append("\tAssert.That(values[").Append(fi++).Append("], Is.EqualTo(").Append(field.Offset)
						.Append(")); // ").AppendLine(field.Name);
				}
				if (@struct.StructureType is { } sType) {
					Source
						.Append("\tAssert.That((uint)(new ").Append(@struct.OutputName)
						.Append("().SType), Is.EqualTo(").Append(sType.Value).AppendLine("));");
				}
			}

			if (@struct.Bitfields.Count == 0) continue;
			using (OpenTestCase(@struct.SpecName + "_Bitfields")) {
				Source
					.Append("\tvar func = (delegate* unmanaged<").Append(@struct.OutputName).Append("*, void>)")
					.Append("NativeTestLib.GetFunction(\"check_").Append(@struct.OutputName).AppendLine("_bitfields\");");
				Source.AppendLine("\tif (func == null) Assert.Fail(\"Could not load native function\");");
				Source.Append('\t').Append(@struct.OutputName).AppendLine(" arg = new();");
				Source.AppendLine("\tfunc(&arg);");
				uint bfi = 1, bfbase = (uint)@struct.Bitfields.Count;
				foreach (var bf in @struct.Bitfields.Values) {
					Source.Append("\tAssert.That((uint)arg.").Append(bf.Name).Append(", Is.EqualTo(").Append(bfi++)
						.AppendLine("));");
					Source.Append("\targ.").Append(bf.Name).Append(" = (").Append(bf.Type.QualifiedName).Append(')')
						.Append(bfi + bfbase).AppendLine(";");
					Source.Append("\tAssert.That((uint)arg.").Append(bf.Name).Append(", Is.EqualTo(")
						.Append(bfi + bfbase).AppendLine("));");
				}
			}
		}

		foreach (var handle in Api.Handles.Values.Where(h => ReferenceEquals(h.Vendor, Vendor))) {
			using (OpenTestCase(handle.SpecName)) {
				Source.Append("\tAssert.That(sizeof(").Append(handle.OutputName).AppendLine("), Is.EqualTo(8));");
				Source.AppendLine("\tnuint one = 1;");
				Source.Append('\t').Append(handle.OutputName).AppendLine(" h = new();");
				Source.Append('\t').Append(handle.OutputName).Append(" h2 = *(").Append(handle.OutputName)
					.AppendLine("*)&one;");
				Source
					.Append("\tAssert.That((uint)h.ObjectType, Is.EqualTo(").Append(handle.ObjectType.Value)
					.AppendLine("));");
				Source.AppendLine("\tAssert.True(h == h);");
				Source.AppendLine("\tAssert.True(h != h2);");
				Source.AppendLine("\tAssert.False(h != h);");
				Source.AppendLine("\tAssert.False(h == h2);");
				Source.AppendLine("\tAssert.False(h);");
				Source.AppendLine("\tAssert.True(h2);");
			}
		}
	}
}
