﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Linq;

namespace LLVK.Templates;


// Template for generating top-level common functionality
internal sealed class CommonTemplate : TemplateBase
{
	protected override void GenerateBody()
	{
		// Write the version fields
		Source.AppendLine("/// <summary><c>VK_HEADER_VERSION</c> version constant.</summary>");
		Source.Append("public const uint VK_HEADER_VERSION = ")
			.Append(Api.SpecVersion.Build)
			.AppendLine(";");
		Source.AppendLine("/// <summary><c>VK_HEADER_VERSION_COMPLETE</c> version constant.</summary>");
		Source.Append("public const uint VK_HEADER_VERSION_COMPLETE = (")
			.Append(Api.SpecVersion.Major)
			.Append(" << 22) | (")
			.Append(Api.SpecVersion.Minor)
			.Append(" << 12) | ")
			.Append(Api.SpecVersion.Build)
			.AppendLine(";");

		// Write the API constants
		Source.AppendLine();
		foreach (var @const in Api.Constants.Values) {
			Source.Append("public const ")
				.Append(@const.Type)
				.Append(' ')
				.Append(@const.Name)
				.Append(" = ")
				.Append(@const.Value)
				.AppendLine(";");
		}

		// Write the docs for the vendor subclasses
		Source.AppendLine();
		foreach (var vendor in VulkanApi.KnownVendors.Values) {
			Source.Append("/// <summary>The Vulkan API bindings for the ")
				.Append(vendor.Tag)
				.AppendLine(" vendor extensions.</summary>");
			Source.Append("public static partial class ")
				.Append(vendor.Tag)
				.AppendLine(" { }");
		}

		// Write IVulkanHandle
		Source
			.AppendLine()
			.AppendLine("/// <summary>Base interface for Vulkan object handle types.</summary>")
			.AppendLine("public interface IVulkanHandle")
			.AppendLine("{")
			.AppendLine("\t/// <summary>The raw numeric handle.</summary>")
			.AppendLine("\tnuint Handle { get; }")
			.AppendLine("\t/// <summary>The object type enumeration value for the handle.</summary>")
			.AppendLine("\tVkObjectType ObjectType { get; }")
			.AppendLine("}");

		// Write IVulkanChainableStruct
		Source
			.AppendLine()
			.AppendLine("/// <summary>Base interface for Vulkan structs with SType and PNext fields for chaining.</summary>")
			.AppendLine("public interface IVulkanChainableStruct")
			.AppendLine("{")
			.AppendLine("\t/// <summary>The type of the structure.</summary>")
			.AppendLine("\tVkStructureType SType { get; set; }")
			.AppendLine("\t/// <summary>The pointer to the next chainable struct.</summary>")
			.AppendLine("\tvoid* PNext { get; set; }")
			.AppendLine("}");

		// Write the chain action delegate for structure chains
		Source.AppendLine();
		Source.AppendLine("/// <summary>Callable action to apply to a structure chain.</summary>");
		Source.AppendLine("/// <remarks>The <c>SType</c> and <c>PNext</c> fields in the chain <em>should not</em> be changed.</remarks>");
		Source.AppendLine("/// <param name=\"first\">A pointer to the first structure in the linked chain.</param>");
		Source.AppendLine("public delegate void StructureChainAction<T>(T* first) where T : unmanaged, IVulkanChainableStruct;");

		// Write the StructureChain types (2-10 types)
		string[] ordinals = { "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th" };
		string[] typeList = { "T0", "T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9" };
		foreach (var typeCount in Enumerable.Range(2, 9)) {
			Source.AppendLine();
			Source.AppendLine("/// <summary>Managed set of Vulkan chainable structure types.</summary>");
			Source.Append("public class VulkanStructureChain<").Append(String.Join(", ", typeList.Take(typeCount))).AppendLine(">");
			for (var ti = 0; ti < typeCount; ++ti) {
				Source.Append("\twhere ").Append(typeList[ti]).AppendLine(" : unmanaged, IVulkanChainableStruct");
			}
			Source.AppendLine("{");
			for (var ti = 0; ti < typeCount; ++ti) {
				Source.Append("\t/// <summary>The ").Append(ordinals[ti]).AppendLine(" struct in the chain.</summary>");
				Source.Append("\tpublic ").Append(typeList[ti]).Append(" Value").Append(ti).AppendLine(" = new();");
			}
			Source.AppendLine();
			Source.AppendLine(
				"\t/// <summary>Creates a chain with the fields, and passes the start of the chain to the provided method.</summary>");
			Source.AppendLine("\tpublic void Invoke(StructureChainAction<T0> action)");
			Source.AppendLine("\t{");
			for (var ti = 0; ti < typeCount; ++ti) {
				Source.Append("\t\tfixed(").Append(typeList[ti]).Append("* t").Append(ti).Append(" = &Value").Append(ti)
					.AppendLine(")");
			}
			Source.AppendLine("\t\t{");
			for (var ti = 0; ti < typeCount - 1; ++ti) {
				Source.Append("\t\t\tt").Append(ti).Append("->PNext = t").Append(ti + 1).AppendLine(";");
			}
			Source.AppendLine("\t\t\taction(t0);");
			Source.AppendLine("\t\t}");
			Source.AppendLine("\t}");
			Source.AppendLine("}");
		}
	}
}
