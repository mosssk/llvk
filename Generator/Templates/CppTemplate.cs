﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Linq;

namespace LLVK.Templates;


// Template for generating C/C++ code for testing native layouts and values
internal sealed class CppTemplate : TemplateBase
{
	protected override bool IsCpp => true;

	protected override void GenerateBody()
	{
		// Generate the struct size and layout functions
		foreach (var @struct in Api.Structs.Values.Where(s => ReferenceEquals(s.Vendor, Vendor))) {
			var sName = @struct.OutputName;
			Source.Append("LLVK_EXPORT U32 get_").Append(sName).AppendLine("_info(U32* fields) {");
			uint fi = 0;
			foreach (var field in @struct.Fields.Values.OrderBy(static f => f.Offset)) {
				if (field.Name.StartsWith("_bit")) continue; // Skip bitfields
				Source.Append("\tfields[").Append(fi++).Append("] = (U32)offsetof(").Append(@struct.OutputName)
					.Append(", ").Append(field.SpecName).AppendLine(");");
			}
			Source.Append("\treturn (U32)sizeof(").Append(sName).AppendLine(");");
			Source.AppendLine("}");
			Source.AppendLine();

			// Generate tests for bitfields
			if (@struct.Bitfields.Count > 0) {
				Source
					.Append("LLVK_EXPORT void check_").Append(sName).Append("_bitfields(").Append(sName)
					.AppendLine("* arg) {");
				uint bfi = 1;
				foreach (var bf in @struct.Bitfields.Values) {
					Source.Append("\targ->").Append(bf.SpecName).Append(" = (").Append(bf.Type.SpecName).Append(')')
						.Append(bfi++).AppendLine(";");
				}
				Source.AppendLine("}");
				Source.AppendLine();
			}
		}

		// Generate the values for enums
		foreach (var @enum in Api.Enums.Values.Where(e => ReferenceEquals(e.Vendor, Vendor))) {
			Source.Append("LLVK_EXPORT U32 get_").Append(@enum.SpecName).AppendLine("_values(I64* values) {");
			uint fi = 0;
			foreach (var field in @enum.Fields.Values.OrderBy(static f => f.Value)) {
				Source.Append("\tvalues[").Append(fi++).Append("] = (I64)").Append(field.SpecName).AppendLine(";");
			}
			Source.Append("\treturn (U32)sizeof(").Append(@enum.SpecName).AppendLine(");");
			Source.AppendLine("}");
			Source.AppendLine();
		}
	}
}
