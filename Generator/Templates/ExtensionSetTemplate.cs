﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Linq;

namespace LLVK.Templates;


// Template for generating the extension set types
internal sealed class ExtensionSetTemplate : TemplateBase
{
	protected override void GenerateUsings()
	{
		Source.AppendLine("using System.Collections.Generic;");
		Source.AppendLine("using System.Text;");
	}

	protected override void GenerateBody()
	{
		var instExtList = Api.Extensions.Values
			.Where(static e => !e.IsDeviceExt)
			.Select(static e => (Ext: e, Name: e.Name[3..]))
			.ToArray();
		var devExtList = Api.Extensions.Values
			.Where(static e => e.IsDeviceExt)
			.Select(static e => (Ext: e, Name: e.Name[3..]))
			.ToArray();

		// Open instance class
		Source.AppendLine("public sealed partial class InstanceExtensionSet");
		Source.AppendLine("{");

		// Generate the body
		genFields(instExtList);
		Source.AppendLine();
		genGet(instExtList);
		Source.AppendLine();
		genSet(instExtList);
		Source.AppendLine();
		genEnumerate(instExtList);
		Source.AppendLine();
		genLoad(instExtList);
		Source.AppendLine();

		// Close instance class
		Source.AppendLine("}").AppendLine();


		// Open device class
		Source.AppendLine("public sealed partial class DeviceExtensionSet");
		Source.AppendLine("{");

		// Generate the body
		genFields(devExtList);
		Source.AppendLine();
		genGet(devExtList);
		Source.AppendLine();
		genSet(devExtList);
		Source.AppendLine();
		genEnumerate(devExtList);
		Source.AppendLine();
		genLoad(devExtList);
		Source.AppendLine();

		// Close device class
		Source.AppendLine("}");
		Source.AppendLine();


		// Open layers class
		Source.AppendLine("public sealed partial class InstanceLayerSet");
		Source.AppendLine("{");

		// Generate the load function
		Source.AppendLine("\t/// <summary>Loads the set of layers available on the current runtime.</summary>");
		Source.AppendLine("\t[SkipLocalsInit]");
		Source.AppendLine("\tpublic static InstanceLayerSet Load()");
		Source.AppendLine("\t{");
		Source.AppendLine("\t\tuint layerCount;");
		Source.AppendLine("\t\tvkEnumerateInstanceLayerProperties(&layerCount, null).ThrowIfError();");
		Source.AppendLine("\t\tvar layers = new Layer[layerCount];");
		Source.AppendLine(
			"\t\tvar layerPtr = (VkLayerProperties*)NativeMemory.Alloc((nuint)(layerCount * sizeof(VkLayerProperties)));");
		Source.AppendLine("\t\ttry {");
		Source.AppendLine("\t\t\tvkEnumerateInstanceLayerProperties(&layerCount, layerPtr).ThrowIfError();");
		Source.AppendLine("\t\t\tfor (uint li = 0; li < layerCount; ++li) {");
		Source.AppendLine(
			"\t\t\t\tlayers[li] = new(Marshal.PtrToStringUTF8(new(&layerPtr[li].LayerName))!, layerPtr[li].ImplementationVersion);");
		Source.AppendLine("\t\t\t}");
		Source.AppendLine("\t\t\treturn new(layers);");
		Source.AppendLine("\t\t}");
		Source.AppendLine("\t\tfinally { NativeMemory.Free(layerPtr); }");
		Source.AppendLine("\t}");

		// Close layers class
		Source.AppendLine("}");

		return;


		void genFields((VulkanApi.FeatureSet.Ext Ext, string Name)[] extList)
		{
			foreach (var ext in extList) {
				Source.Append("\tpublic bool ").Append(ext.Name).AppendLine(";");
			}
		}

		void genGet((VulkanApi.FeatureSet.Ext Ext, string Name)[] extList)
		{
			Source.AppendLine("\tpublic partial bool Get(string extName) => extName switch {");
			foreach (var ext in extList) {
				var extNameField = $"{(ext.Ext.Vendor is { } v ? $"{v.Tag}." : "")}{ext.Ext.ExtNameName}";
				Source.Append("\t\t").Append(extNameField).Append(" => ").Append(ext.Name).AppendLine(",");
			}
			Source.AppendLine(
				"\t\t_ => throw new ArgumentException($\"Invalid extension '{extName}'\", nameof(extName))");
			Source.AppendLine("\t};");
		}

		void genSet((VulkanApi.FeatureSet.Ext Ext, string Name)[] extList)
		{
			Source.AppendLine("\tpublic partial bool Set(string extName, bool isSet) => extName switch {");
			foreach (var ext in extList) {
				var extNameField = $"{(ext.Ext.Vendor is { } v ? $"{v.Tag}." : "")}{ext.Ext.ExtNameName}";
				Source.Append("\t\t").Append(extNameField).Append(" => ").Append(ext.Name).AppendLine(" = isSet,");
			}
			Source.AppendLine(
				"\t\t_ => throw new ArgumentException($\"Invalid extension '{extName}'\", nameof(extName))");
			Source.AppendLine("\t};");
		}

		void genEnumerate((VulkanApi.FeatureSet.Ext Ext, string Name)[] extList)
		{
			Source.AppendLine("\tpublic partial IEnumerable<string> EnumerateSet() {");
			foreach (var ext in extList) {
				var extNameField = $"{(ext.Ext.Vendor is { } v ? $"{v.Tag}." : "")}{ext.Ext.ExtNameName}";
				Source.Append("\t\tif (").Append(ext.Name).Append(") yield return ").Append(extNameField)
					.AppendLine(";");
			}
			Source.AppendLine("\t}");
		}

		void genLoad((VulkanApi.FeatureSet.Ext Ext, string Name)[] extList)
		{
			var isDev = extList[0].Ext.IsDeviceExt;
			var setName = isDev ? "DeviceExtensionSet" : "InstanceExtensionSet";
			var argList = isDev ? "VkPhysicalDevice device" : "";

			// Method open
			if (isDev) {
				Source.AppendLine("\t/// <summary>Loads the set of extensions supported by the device.</summary>");
				Source.AppendLine("\t/// <remarks>Requires <see cref=\"VulkanLoader.LoadInstanceFunctions\"/> to have been called.</remarks>");
				Source.AppendLine("\t/// <param name=\"device\">The device to load the extensions for.</param>");
				Source.AppendLine("\t/// <exception cref=\"ArgumentException\">The given device was null.</exception>");
			}
			else {
				Source.AppendLine("\t/// <summary>Loads the set of extensions supported by the current runtime.</summary>");
			}
			Source.AppendLine("\t[SkipLocalsInit]");
			Source.Append("\tpublic static ").Append(setName).Append(" Load(").Append(argList).AppendLine(")");
			Source.AppendLine("\t{");

			// Arg check
			if (isDev) {
				Source.AppendLine(
					"\t\tif (!device) throw new ArgumentException(\"Cannot pass a null device\", nameof(device));");
			}
			Source.Append("\t\t").Append(setName).AppendLine(" set = new();");
			Source.AppendLine();

			// Load the extensions
			Source.AppendLine("\t\tuint extCount;");
			Source.AppendLine(isDev
				? "\t\tvkEnumerateDeviceExtensionProperties(device, null, &extCount, null).ThrowIfError();"
				: "\t\tvkEnumerateInstanceExtensionProperties(null, &extCount, null).ThrowIfError();");
			Source.AppendLine(
				"\t\tvar extPtr = (VkExtensionProperties*)NativeMemory.Alloc((nuint)(extCount * sizeof(VkExtensionProperties)));");
			Source.AppendLine("\t\ttry {");
			Source.AppendLine(isDev
				? "\t\t\tvkEnumerateDeviceExtensionProperties(device, null, &extCount, extPtr).ThrowIfError();"
				: "\t\t\tvkEnumerateInstanceExtensionProperties(null, &extCount, extPtr).ThrowIfError();");

			// Apply the extensions
			Source.AppendLine("\t\t\tfor (uint ei = 0; ei < extCount; ++ei) {");
			Source.AppendLine("\t\t\t\tswitch (Marshal.PtrToStringUTF8(new(&extPtr[ei].ExtensionName)))");
			Source.AppendLine("\t\t\t\t{");
			foreach (var ext in extList) {
				var extNameField = $"{(ext.Ext.Vendor is { } v ? $"{v.Tag}." : "")}{ext.Ext.ExtNameName}";
				Source.Append("\t\t\t\tcase ").Append(extNameField).Append(": set.").Append(ext.Name)
					.AppendLine(" = true; break;");
			}
			Source.AppendLine("\t\t\t\t}");
			Source.AppendLine("\t\t\t}");

			// Method close
			Source.AppendLine("\t\t}");
			Source.AppendLine("\t\tfinally { NativeMemory.Free(extPtr); }");
			Source.AppendLine();
			Source.AppendLine("\t\treturn set;");
			Source.AppendLine("\t}");
		}
	}
}
