﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace LLVK.Templates;


// Template for generating API helper, utility, and extension functionality
internal sealed class HelperTemplate : TemplateBase
{
	protected override string ClassName => "VulkanHelper";

	protected override void GenerateUsings() => Source.AppendLine("using static LLVK.Vulkan;");

	protected override void GenerateBody()
	{
		// VkResult classifications
		Source
			.AppendLine("/// <summary>Gets if the <see cref=\"VkResult\"/> value represents an error.</summary>")
			.AppendLine("/// <remarks>Error results are all results with negative numeric values.</remarks>")
			.AppendLine("[MethodImpl(MethodImplOptions.AggressiveInlining)]")
			.AppendLine("public static bool IsError(this VkResult result) => (int)result < 0;")
			.AppendLine();
		Source
			.AppendLine("/// <summary>Gets if the <see cref=\"VkResult\"/> value is the success result.</summary>")
			.AppendLine("[MethodImpl(MethodImplOptions.AggressiveInlining)]")
			.AppendLine("public static bool IsSuccess(this VkResult result) => result == VkResult.Success;")
			.AppendLine();
		Source
			.AppendLine("/// <summary>Gets if the <see cref=\"VkResult\"/> value represents an indeterminate result.</summary>")
			.AppendLine("/// <remarks>Indeterminate results are all results with positive, non-zero numeric values.</remarks>")
			.AppendLine("[MethodImpl(MethodImplOptions.AggressiveInlining)]")
			.AppendLine("public static bool IsIndeterminate(this VkResult result) => (int)result > 0;")
			.AppendLine();

		// VkResult throw
		Source
			.AppendLine("/// <summary>Throws an exception if the <see cref=\"VkResult\"/> value represents an error.</summary>")
			.AppendLine("/// <remarks>Does not throw for results that are indeterminate (numeric value &gt; 0).</remarks>")
			.AppendLine("[MethodImpl(MethodImplOptions.NoInlining)]")
			.AppendLine("public static void ThrowIfError(this VkResult result)")
			.AppendLine("{")
			.AppendLine("\tif (result.IsError()) throw new ResultException(result);")
			.AppendLine("}")
			.AppendLine();

		// VkResult exception type
		Source
			.AppendLine("/// <summary>Exception that is thrown when an error <see cref=\"VkResult\"/> occurs.</summary>")
			.AppendLine("public sealed class ResultException : Exception")
			.AppendLine("{")
			.AppendLine("\t/// <summary>The result value that generated the exception.</summary>")
			.AppendLine("\tpublic readonly VkResult Result;")
			.AppendLine()
			.AppendLine("\tinternal ResultException(VkResult result) : base($\"Vulkan error: {result}\") => Result = result;")
			.AppendLine("}");
	}
}
