﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace LLVK;


// Base class for types that are defined in the Vulkan API spec
internal abstract class ApiType
{
	#region Fields
	// The type name as it appears in the spec
	public abstract string SpecName { get; }
	// The type name as it is generated in the spec bindings
	public abstract string OutputName { get; }
	// The size of the type (in bytes)
	public abstract uint TypeSize { get; }
	// The alignment requirements of the type
	public abstract uint TypeAlignment { get; }

	// The vendor for the type
	public virtual VulkanApi.Vendor? Vendor { get; set; }

	// The fully qualified output name with the vendor
	public string QualifiedName => Vendor != null ? Vendor.Tag + "." +  OutputName : OutputName;
	#endregion // Fields

	private ApiType() { }


	// Special type representing an unresolved type
	public sealed class Unresolved : ApiType
	{
		public override string SpecName => "UNRESOLVED";
		public override string OutputName => "UNRESOLVED";
		public override uint TypeSize => 0;
		public override uint TypeAlignment => 0;

		public readonly string TypeName; // The base name of the unresolved type
		public readonly uint PtrDepth;   // Pointer depth of the type
		public readonly uint ArraySize;  // Array size of the type

		public Unresolved(string name, uint ptrDepth, uint arraySize) =>
			(TypeName, PtrDepth, ArraySize) = (name, ptrDepth, arraySize);
	}


	// Represents special types, such as platform-specific types, special zero-sized types, or hand-crafted types
	//    These are the only types allowed to have zero size, necessary for opaque platform types
	public sealed class Special : ApiType
	{
		public override string SpecName { get; }
		public override string OutputName { get; }
		public override uint TypeSize { get; }
		public override uint TypeAlignment { get; }

		public Special(string specName, string outputName, uint typeSize, uint? typeAlignment = null)
		{
			SpecName = specName;
			OutputName = outputName;
			TypeSize = typeSize;
			TypeAlignment = typeAlignment.GetValueOrDefault(typeSize);
		}
	}


	// Represents a numeric type
	public sealed class Numeric : ApiType
	{
		public override string SpecName { get; }
		public override string OutputName { get; }
		public override uint TypeSize { get; }
		public override uint TypeAlignment { get; }

		public Numeric(string specName, string outputName, uint typeSize, uint? typeAlignment = null)
		{
			SpecName = specName;
			OutputName = outputName;
			TypeSize = typeSize;
			TypeAlignment = typeAlignment.GetValueOrDefault(typeSize);
		}
	}


	// Represents a Vulkan object handle
	public sealed class Handle : ApiType
	{
		public override string SpecName { get; }
		public override string OutputName { get; }
		public override uint TypeSize => 8;
		public override uint TypeAlignment => 8;

		public readonly Enums.Field ObjectType;
		public Handle? ParentHandle;

		public Handle(string name, Enums.Field objectType) =>
			(SpecName, OutputName, ObjectType) = (name, name, objectType);

		// If the handle is VkDevice or a child type
		public bool IsDeviceOrChild {
			get {
				var handle = this;
				do {
					if (handle.OutputName == "VkDevice") return true;
				} while ((handle = handle!.ParentHandle) is not null);
				return false;
			}
		}

		// If the handle is VkInstance or a child type that is not also a child of VkDevice
		public bool IsInstanceOrNonDeviceChild {
			get {
				var handle = this;
				do {
					if (handle.OutputName == "VkDevice") return false;
				} while ((handle = handle!.ParentHandle) is not null);
				return true;
			}
		}
	}


	// Represents a function pointer (because these are not parsed, they have a different construction than most)
	public sealed class FuncPointer : ApiType
	{
		// A named argument
		public sealed record Arg(string TypeName, string Name);

		public override string SpecName { get; }
		public override string OutputName { get; }
		public override uint TypeSize => 8;
		public override uint TypeAlignment => 8;

		public readonly string ReturnValue;
		public readonly IReadOnlyList<Arg> Args;

		public FuncPointer(string specName, string retVal, IReadOnlyList<Arg> args)
		{
			SpecName = specName;
			ReturnValue = retVal;
			Args = args;
			OutputName = $"delegate* unmanaged<{String.Join(", ", args.Select(static a => a.TypeName).Append(retVal))}>";
		}
	}


	// Represents a pointer to a type
	public sealed class Pointer : ApiType
	{
		// Special void* type
		public static readonly Pointer VoidPtr = new(new Special("void", "void", 0, 0), 1);

		public override string SpecName { get; }
		public override string OutputName { get; }
		public override uint TypeSize => 8;
		public override uint TypeAlignment => 8;
		public readonly ApiType BaseType; // The base type without pointers
		public readonly uint PtrDepth;    // The number of pointer indirections (always >= 1)

		public override VulkanApi.Vendor? Vendor { get => BaseType.Vendor; set { } }

		public Pointer(ApiType baseType, uint ptrDepth)
		{
			Debug.Assert(ptrDepth >= 1);
			BaseType = baseType;
			PtrDepth = ptrDepth;
			SpecName = $"{baseType.SpecName}{new string('*', (int)ptrDepth)}";
			OutputName = $"{baseType.OutputName}{new string('*', (int)ptrDepth)}";
		}
	}


	// Represents an inline array of a type
	public sealed class Array : ApiType
	{
		public override string SpecName { get; }
		public override string OutputName { get; }
		public override uint TypeSize => BaseType.TypeSize * ArraySize;
		public override uint TypeAlignment => BaseType.TypeAlignment;
		public readonly ApiType BaseType;
		public readonly uint ArraySize;

		public Array(ApiType baseType, uint arraySize)
		{
			Debug.Assert(arraySize > 1);
			BaseType = baseType;
			ArraySize = arraySize;
			SpecName = $"{baseType.SpecName}[{arraySize}]";
			OutputName = $"{baseType.OutputName}Array_{ArraySize}";
		}
	}


	// Represents an enum or bitmask
	public sealed class Enums : ApiType
	{
		// Represents an enum field (Value can be null if an alias is not resolved)
		public sealed record Field(string SpecName, string OutputName, long? Value, string? Alias)
		{
			public override int GetHashCode() => SpecName.GetHashCode();
		}

		public readonly bool IsBitmask;       // If the enum is a bitmask ("...Flags...")
		public readonly bool IsWide;          // If the enum is 8 bytes instead of 4
		public readonly string FieldNameRoot; // The root of the field names to correctly normalize names
		public override string SpecName { get; }
		public override string OutputName { get; }
		public override uint TypeSize => IsWide ? 8u : 4u;
		public override uint TypeAlignment => IsWide ? 8u : 4u;

		public Dictionary<string, Field> Fields => Alias?.Fields ?? _fields; // Output Name -> Field map
		private readonly Dictionary<string, Field> _fields = new();

		public readonly Enums? Alias;

		public Enums(string specName, string fieldNameRoot, bool isBitmask, bool isWide) =>
			(IsBitmask, IsWide, FieldNameRoot, SpecName, OutputName, Alias) =
			(isBitmask, isWide, fieldNameRoot, specName, specName, null);

		public Enums(string specName, string fieldNameRoot, Enums alias) =>
			(IsBitmask, IsWide, FieldNameRoot, SpecName, OutputName, Alias) =
			(alias.IsBitmask, alias.IsWide, fieldNameRoot, specName, specName, alias);
	}


	// Represents a struct
	public sealed class Struct : ApiType
	{
		// Represents a struct field (Type may be unresolved until build)
		public sealed record Field(string Name, string SpecName, ApiType Type, uint Offset, uint Order);
		// Represents a bitfield member (offset and size in bits)
		public sealed record Bitfield(string Name, string SpecName, ApiType Type, uint Number, uint Offset, uint Size);

		private static readonly Numeric _UInt32 = new("uint32_t", "uint", 4);

		public override string SpecName { get; }
		public override string OutputName { get; }
		public override uint TypeSize => Alias?._typeSize ?? _typeSize;
		public override uint TypeAlignment => Alias?._typeAlignment ?? _typeAlignment;
		public readonly bool IsUnion;
		public readonly bool IsReturnedOnly;

		public bool GenCtor => Alias?.GenCtor ?? _genCtor ?? false;
		public bool IsChainable => Alias?.IsChainable ?? _isChainable ?? false;
		private bool? _genCtor, _isChainable;

		private uint _typeSize;
		private uint _typeAlignment;

		private uint _bitfieldNum; // The current bitfield number
		private uint _bitfieldRem; // Remaining bitfield size
		private uint _bitfieldOffset; // Offset into current bitfield

		public Enums.Field? StructureType {
			get => _structureType ?? Alias?.StructureType;
			set => _structureType = value;
		}
		private Enums.Field? _structureType;

		public readonly Struct? Alias;

		public Dictionary<string, Field> Fields => Alias?.Fields ?? _fields!;
		private readonly Dictionary<string, Field>? _fields;

		public Dictionary<string, Bitfield> Bitfields => Alias?.Bitfields ?? _bitfields!;
		private readonly Dictionary<string, Bitfield>? _bitfields;

		public bool IsBuilt => Alias?.IsBuilt ?? _isBuilt;
		private bool _isBuilt;


		public Struct(string name, bool union, bool rOnly) =>
			(SpecName, OutputName, IsUnion, IsReturnedOnly, _fields, _bitfields, Alias) =
			(name, name, union, rOnly, new(), new(), null);
		public Struct(string name, Struct alias) =>
			(SpecName, OutputName, IsUnion, IsReturnedOnly, _fields, _bitfields, Alias) =
			(name, name, alias.IsUnion, alias.IsReturnedOnly, null, null, alias);


		// Adds a field to the struct
		public void AddField(string name, ApiType type, uint? bitfieldSize)
		{
			if (Alias is not null) throw new SpecError($"Attempted to add field to alias struct '{SpecName}'");
			if (IsBuilt) throw new SpecError($"Cannot build struct '{SpecName}' more than once");

			// Normalize the field name to match C# conventions
			var specName = name;
			name = type is Pointer { PtrDepth: var ptrDepth } && name.StartsWith(new string('p', (int)ptrDepth))
				? (new string('P', (int)ptrDepth) + name[(int)ptrDepth..])
				: (Char.ToUpperInvariant(name[0]) + name[1..]);
			switch (name) {
			case "SType" when type.OutputName == "VkStructureType": name = "_sType"; break;
			case "PNext" when type.OutputName == "void*":           name = "_pNext"; break;
			}

			// Handle bitfields specially
			if (bitfieldSize is { } bfs) {
				if (type is not Enums { IsWide: false } && type.OutputName != "uint") {
					throw new SpecError($"Bitfield type '{type}' was not uint or 4-byte enum");
				}
				if (_bitfieldRem < bfs) { // Need a new backing field for the bitfield
					_bitfieldRem = type.TypeSize * 8;
					_bitfieldOffset = 0;
					_bitfieldNum += 1;
					var bfName = $"_bitfield{_bitfieldNum}";
					_fields!.Add(bfName, new(bfName, bfName, _UInt32, 0, (uint)_fields.Count));
				}
				_bitfields!.Add(name, new(name, specName, type, _bitfieldNum, _bitfieldOffset, bfs));
				_bitfieldRem -= bfs;
				_bitfieldOffset += bfs;
			}
			else {
				_fields!.Add(name, new(name, specName, type, 0, (uint)_fields.Count));
				_bitfieldRem = 0;
			}
		}

		// Builds the struct into the final state with correct information
		public void Build(VulkanApi api)
		{
			if (IsBuilt) return;
			if (Alias is not null) {
				Alias.Build(api);
				return;
			}
			_isBuilt = true; // Goes here to prevent stack overflow with self-referencing types

			// Set the field offsets and overall type size and alignment
			foreach (var (k, v) in _fields!.OrderBy(static p => p.Value.Order)) {
				// Resolve field if needed
				var vType = v.Type;
				switch (vType) {
				case Unresolved us: {
					var fTypeName = us.TypeName.Replace("FlagBits", "Flags");
					if (!api.TryGetType(fTypeName, out var realType)) {
						throw new SpecError($"Field '{SpecName}.{k}' is unresolvable type '{fTypeName}'");
					}
					if (realType is Struct rs) rs.Build(api);
					vType = realType;
					if (us.PtrDepth > 0) vType = new Pointer(vType, us.PtrDepth);
					if (us.ArraySize > 1) vType = new Array(vType, us.ArraySize);
				} break;
				case Struct vs: vs.Build(api); break;
				}

				// Update field, type size, and type alignment
				if (IsUnion) {
					_fields![k] = v with { Offset = 0, Type = vType };
					var vSize = alignTo(vType.TypeSize, vType.TypeAlignment);
					if (vSize > _typeSize) {
						_typeSize = vSize;
					}
					if (vType.TypeAlignment > _typeAlignment) {
						_typeAlignment = vType.TypeAlignment;
					}
				}
				else {
					_typeSize = alignTo(_typeSize, vType.TypeAlignment);
					_fields![k] = v with { Offset = _typeSize, Type = vType };
					_typeSize += alignTo(vType.TypeSize, vType.TypeAlignment);
					if (vType.TypeAlignment > _typeAlignment) {
						_typeAlignment = vType.TypeAlignment;
					}
				}
			}

			// Calc if gen ctor
			_genCtor = !IsReturnedOnly && Fields.Count <= 6 &&
				Fields.Values.All(static f => f.Type is Numeric or Enums or Struct { GenCtor: true } or Handle);
			_isChainable =
				Fields.Values.Any(static f => f is { Name: "_sType", Order: 0 }) &&
				Fields.Values.Any(static f => f is { Name: "_pNext", Order: 1 });

			_typeSize = alignTo(_typeSize, _typeAlignment);
			return;

			// Align (round up) a size or offset to a given size
			static uint alignTo(uint size, uint align) => (size % align == 0) ? size : (size + align - (size % align));
		}
	}
}
