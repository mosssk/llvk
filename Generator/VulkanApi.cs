﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace LLVK;


// Contains the repository of all macros, constants, defines, types, enums, and functions parsed from the API
internal sealed class VulkanApi
{
	// Known vendors
	public static readonly IReadOnlyDictionary<string, Vendor> KnownVendors = new Dictionary<string, Vendor> {
		{ "IMG", new("IMG") },
        { "AMD", new("AMD") }, { "AMDX", new("AMDX") },
        { "ARM", new("ARM") },
        { "FSL", new("FSL") },
        { "BRCM", new("BRCM") },
        { "NXP", new("NXP") },
        { "NV", new("NV") }, { "NVX", new("NVX") },
        { "VIV", new("VIV") },
        { "VSI", new("VSI") },
        { "KDAB", new("KDAB") },
        { "ANDROID", new("ANDROID") }, { "CHROMIUM", new("CHROMIUM") }, { "FUCHSIA", new("FUCHSIA") },
        { "GGP", new("GGP") }, { "GOOGLE", new("GOOGLE") },
        { "QCOM", new("QCOM") },
        { "LUNARG", new("LUNARG") },
        { "SAMSUNG", new("SAMSUNG") }, { "SEC", new("SEC") }, { "TIZEN", new("TIZEN") },
        { "RENDERDOC", new("RENDERDOC") },
        { "NN", new("NN") },
        { "MVK", new("MVK") },
        { "KHR", new("KHR") }, { "KHX", new("KHX") },
        { "EXT", new("EXT") },
        { "MESA", new("MESA") },
        { "INTEL", new("INTEL") },
        { "HUAWEI", new("HUAWEI") },
        { "VALVE", new("VALVE") },
        { "QNX", new("QNX") },
        { "JUICE", new("JUICE") },
        { "FB", new("FB") }
	};

	// Core feature sets
	public static readonly FeatureSet.Core Core10 = new(1, 0);
	public static readonly FeatureSet.Core Core11 = new(1, 1);
	public static readonly FeatureSet.Core Core12 = new(1, 2);


	#region Fields
	// The expected spec version
	public readonly Version SpecVersion;

	// Known API constants
	public readonly Dictionary<string, Constant> Constants = new();

	// Known basic types (from includes or defines)
	public readonly Dictionary<string, ApiType> BaseTypes = new();

	// Known extensions
	public readonly Dictionary<string, FeatureSet.Ext> Extensions = new();

	// Collections of unassigned feature set types and commands
	public readonly Dictionary<string, FeatureSet> FeatureTypes = new(); // Also contains func pointers
	public readonly Dictionary<string, FeatureSet> FeatureCommands = new();

	// Names of features from ignored feature sets
	public readonly HashSet<string> DisabledFeatures = new() { "VkBaseInStructure", "VkBaseOutStructure" };

	// Known enums and bitmasks
	public readonly Dictionary<string, ApiType.Enums> Enums = new();

	// Known function pointers
	public readonly Dictionary<string, ApiType.FuncPointer> FuncPointers = new();

	// Known handle types
	public readonly Dictionary<string, ApiType.Handle> Handles = new();

	// Known structs
	public readonly Dictionary<string, ApiType.Struct> Structs = new();

	// Known functions
	public readonly Dictionary<string, Function> Functions = new();
	#endregion // Fields


	public VulkanApi(Version specVersion) => SpecVersion = specVersion;


	// Attempts to find the given spec type
	public bool TryGetType(string specName, [NotNullWhen(true)] out ApiType? type)
	{
		type =
			BaseTypes.TryGetValue(specName, out var basicType) ? basicType :
			Enums.TryGetValue(specName, out var enumsType) ? enumsType :
			FuncPointers.TryGetValue(specName, out var fnptrType) ? fnptrType :
			Handles.TryGetValue(specName, out var handleType) ? handleType :
			Structs.TryGetValue(specName, out var structType) ? structType :
			null;
		return type is not null;
	}


	#region Name Utils
	// Extracts the vendor tag from the identifier suffix (all ending caps matching known vendor)
	public static Vendor? GetVendorForName(ReadOnlySpan<char> name)
	{
		var count = 0;
		for (var i = name.Length - 1; i >= 0; --i) {
			if (!Char.IsUpper(name[i])) break;
			count += 1;
		}
		if (count == 0) return null;
		return KnownVendors.TryGetValue(name[^count..].ToString(), out var vendor) ? vendor : null;
	}

	// Normalizes an enum or bitmask type name, and extracts additional information
	//   specName: The original specification name
	//   isBitmask: If the type is a bitmask (false == normal enum)
	//   vendor: The vendor of the enum if detected (throws if a vendor is present but is unknown)
	//   fieldNameRoot: The root name of the PascalCase fields in the enum
	public static (Vendor? Vendor, string FieldRoot) ExtractEnumTypeInfo(string specName, bool isBitmask)
	{
		// Get the vendor
		var vendor = GetVendorForName(specName);
		var vendorLen = vendor?.Tag.Length ?? 0;

		// Normalized name
		var isVersioned = Char.IsDigit(specName[^(1 + vendorLen)]);
		var baseName = specName.AsSpan()[..^(vendorLen + (isVersioned ? 1 : 0))];
		if (isBitmask && !baseName.EndsWith("Flags")) {
			throw new SpecError($"Bitmask without 'Flags' suffix: {specName}");
		}

		// Field root
		var fieldRoot = specName.Replace("Flags", "");
		if (vendorLen > 0) fieldRoot = fieldRoot[..^vendorLen];

		return (vendor, fieldRoot);
	}

	// Normalizes an enum value name given the enum type name
	public static string NormalizeEnumValueName(ApiType.Enums @enum, string specName)
	{
		var trimEnd = 0;

		// Temp remove vendor
		var vendor = GetVendorForName(specName);
		if (vendor is not null) {
			if (specName[^(vendor.Tag.Length + 1)] != '_') {
				throw new SpecError($"Enum value name '{specName}' is missing _ before vendor");
			}
			trimEnd += (vendor.Tag.Length + 1); // Skip _VENDOR
		}

		// Remove _BIT from bitmasks
		if (@enum.IsBitmask && specName.AsSpan()[..^trimEnd].EndsWith("_BIT")) {
			trimEnd += "_BIT".Length;
		}

		// Normalize to PascalCase
		var nameLen = specName.Length - trimEnd;
		Span<char> pascalCase = stackalloc char[nameLen];
		var nextCaps = true;
		var oi = 0;
		for (var ii = 0; ii < nameLen; ++ii) {
			var ch = specName[ii];
			if (ch == '_') {
				nextCaps = true;
				continue;
			}
			if (Char.IsLetter(ch)) {
				pascalCase[oi++] = nextCaps ? Char.ToUpperInvariant(ch) : Char.ToLowerInvariant(ch);
				nextCaps = false;
			}
			else if (Char.IsDigit(ch)) {
				pascalCase[oi++] = ch;
			}
			else throw new SpecError($"Unknown character '{ch}' in enum value '{specName}'");
		}

		// Trim the common prefix
		var preLen = 0;
		var maxLen = Math.Min(oi, @enum.FieldNameRoot.Length);
		while (preLen < maxLen && pascalCase[preLen] == @enum.FieldNameRoot[preLen]) preLen++;

		// Fix names that start with a digit or lowercase
		var prefix = Char.IsDigit(pascalCase[preLen]) ? "E" : "";
		if (Char.IsLower(pascalCase[preLen])) {
			prefix += Char.ToUpperInvariant(pascalCase[preLen]);
			preLen += 1;
		}

		// Prefix + Pascal case name + vendor tag if present
		return prefix + pascalCase.Slice(preLen, oi - preLen).ToString() + (vendor?.Tag ?? "");
	}
	#endregion // Name Utils


	// Vendor/Author tags
	public sealed record Vendor(string Tag)
	{
		public override int GetHashCode() => Tag.GetHashCode();
	}

	// Describes a feature set that part of the API can belong to (core features or extension)
	public abstract record FeatureSet
	{
		private FeatureSet() { }

		// Core feature set
		public sealed record Core(uint Major, uint Minor) : FeatureSet
		{
			public override string ToString() => $"Core({Major}.{Minor})";
		}

		// Spec extension
		public sealed record Ext(
			string Name, uint Number, string ExtNameName, string ExtNameValue,
			string ExtVersionName, uint ExtVersionValue, bool IsDeviceExt, Vendor Vendor
		) : FeatureSet
		{
			public override string ToString() => Name;
		}
	}

	// An API constant (Value contains the sanitized constant value, SpecValue is the raw value from the spec)
	public sealed record Constant(string Name, string Type, string Value, string SpecValue);

	// A command/function for the API
	public sealed record Function(
		string Name, ApiType ReturnType, (ApiType Type, string Name)[] Args, Vendor? Vendor = null
	)
	{
		// All global functions defined by the spec (rarely changes) + "vkGet...ProcAddr"
		public bool IsGlobal => Name is "vkEnumerateInstanceVersion" or "vkEnumerateInstanceExtensionProperties" or
			"vkEnumerateInstanceLayerProperties" or "vkCreateInstance" or "vkGetInstanceProcAddr" or
			"vkGetDeviceProcAddr";

		// All instance-level dispatchable functions
		public bool IsInstance => !IsGlobal && ((ApiType.Handle)Args[0].Type).IsInstanceOrNonDeviceChild;

		// All device-level dispatchable functions
		public bool IsDevice => !IsGlobal && ((ApiType.Handle)Args[0].Type).IsDeviceOrChild;
	}
}
