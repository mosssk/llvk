﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LLVK.Templates;

namespace LLVK;


// Handles generating the C# bindings and tests for the API
internal static class SpecGenerator
{
	// The templates
	private static readonly CommonTemplate _CommonTemplate = new();
	private static readonly CommonTestsTemplate _CommonTestsTemplate = new();
	private static readonly LoaderTemplate _LoaderTemplate = new();
	private static readonly VendorTemplate _VendorTemplate = new();
	private static readonly VendorTestsTemplate _VendorTestsTemplate = new();
	private static readonly HelperTemplate _HelperTemplate = new();
	private static readonly HelperTestsTemplate _HelperTestTemplate = new();
	private static readonly ExtensionSetTemplate _ExtensionSetTemplate = new();
	private static readonly CppTemplate _CppTemplate = new();


	// Performs all of the generation for the given API repo
	public static async Task GenerateApi(VulkanApi api, FileInfo specProject, FileInfo testProject,
		CancellationToken token)
	{
		// Setup the generation directories
		var specDir = new DirectoryInfo(Path.Combine(specProject.Directory!.FullName, "Generated"));
		if (specDir.Exists) specDir.Delete(true);
		specDir.Create();
		var testDir = new DirectoryInfo(Path.Combine(testProject.Directory!.FullName, "Generated"));
		if (testDir.Exists) testDir.Delete(true);
		testDir.Create();

		// Generate the top-level common functionality
		Log.Info("Generating common functionality");
		if (_CommonTemplate.Generate(api, null, token)) {
			await _CommonTemplate.WriteToFileAsync(Path.Combine(specDir.FullName, "Vulkan.gen.cs"), token);
		}
		if (_CommonTestsTemplate.Generate(api, null, token)) {
			await _CommonTestsTemplate.WriteToFileAsync(Path.Combine(testDir.FullName, "Vulkan.gen.cs"), token);
		}
		token.ThrowIfCancellationRequested();

		// Generate the loader functionality
		Log.Info("Generating loader functionality");
		if (_LoaderTemplate.Generate(api, null, token)) {
			await _LoaderTemplate.WriteToFileAsync(Path.Combine(specDir.FullName, "VulkanLoader.gen.cs"), token);
		}
		token.ThrowIfCancellationRequested();

		// Generate the helper functionality
		Log.Info("Generating helper functionality");
		if (_HelperTemplate.Generate(api, null, token)) {
			await _HelperTemplate.WriteToFileAsync(Path.Combine(specDir.FullName, "VulkanHelper.gen.cs"), token);
		}
		if (_HelperTestTemplate.Generate(api, null, token)) {
			await _HelperTestTemplate.WriteToFileAsync(Path.Combine(testDir.FullName, "VulkanHelper.gen.cs"), token);
		}
		token.ThrowIfCancellationRequested();

		// Generate the extension set types
		Log.Info("Generating extension set functionality");
		if (_ExtensionSetTemplate.Generate(api, null, token)) {
			await _ExtensionSetTemplate.WriteToFileAsync(Path.Combine(specDir.FullName, "ExtensionSets.gen.cs"), token);
		}
		token.ThrowIfCancellationRequested();

		// Generate the core and vendor functionality
		Directory.CreateDirectory(Path.Combine(testDir.FullName, "cpp"));
		foreach (var vendor in VulkanApi.KnownVendors.Values.Prepend(null)) {
			Log.Info($"Generating {vendor?.Tag ?? "Core"} Types");
			var fileName = $"{vendor?.Tag ?? "Core"}.gen.cs";
			var cppFileName = $"{vendor?.Tag ?? "Core"}.gen.cpp";
			if (_VendorTemplate.Generate(api, vendor, token)) {
				await _VendorTemplate.WriteToFileAsync(Path.Combine(specDir.FullName, fileName), token);
			}
			if (_VendorTestsTemplate.Generate(api, vendor, token)) {
				await _VendorTestsTemplate.WriteToFileAsync(Path.Combine(testDir.FullName, fileName), token);
			}
			if (_CppTemplate.Generate(api, vendor, token)) {
				await _CppTemplate.WriteToFileAsync(Path.Combine(testDir.FullName, "cpp", cppFileName), token);
			}
		}
	}
}
