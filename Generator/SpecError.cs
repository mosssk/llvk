﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace LLVK;


// Represents a handled error while visiting the API files
internal sealed class SpecError : Exception
{
	public SpecError() : base("Unknown visitor error") { }
	public SpecError(string msg) : base(msg) { }
	public SpecError(string msg, Exception inner) : base(msg, inner) { }
}
