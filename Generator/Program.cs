﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.CommandLine;
using System.IO;
using System.Threading;
using LLVK;


// Parse the command line, returning immediately if error
if (parseCommandLine(args) is not { } opts) {
	return -1;
}

// Report
Log.Info($"XML Spec File:  {opts.SpecFile.FullName}");
Log.Info($"LLVK Library:   {opts.LibProj.FullName}");
Log.Info($"LLVK Tests:     {opts.TestProj.FullName}");

// Cancellation support
CancellationTokenSource cancelSource = new();
Console.CancelKeyPress += (_, evt) => {
	Log.Warn("Signal received, attempting to cancel...");
	cancelSource.Cancel(true);
	evt.Cancel = true;
};

// Process the core vulkan spec
VulkanApi api;
try {
	api = await SpecParser.ParseAsync(opts.SpecVersion, opts.SpecFile, cancelSource.Token);
	cancelSource.Token.ThrowIfCancellationRequested();
}
catch (OperationCanceledException) {
	Log.Error("Error: Processing cancelled by user");
	return -2;
}
catch (SpecError err) {
	Log.Error($"Error: {err.Message}");
	return -3;
}
catch (Exception ex) {
	Log.Error($"Error: unhandled processing exception ('{ex.GetType()}') - {ex.Message}");
	Log.Error(ex.StackTrace ?? "No Stack Trace");
	return -4;
}

// Generate the specification
try {
	await SpecGenerator.GenerateApi(api, opts.LibProj, opts.TestProj, cancelSource.Token);
}
catch (OperationCanceledException) {
	Log.Error("Error: Generating cancelled by user");
	return -5;
}
catch (IOException ex) {
	Log.Error($"Error: Failed to generate specification files - {ex.Message}");
	return -6;
}
catch (Exception ex) {
	Log.Error($"Error: unhandled generation exception ('{ex.GetType()}') - {ex.Message}");
	Log.Error(ex.StackTrace ?? "No Stack Trace");
	return -7;
}

// Done
return 0;


// Parses the command line
static (FileInfo SpecFile, Version SpecVersion, FileInfo LibProj, FileInfo TestProj)? parseCommandLine(string[] args)
{
	// Define the options
	var specFileOption = new Option<FileInfo>("-i", "The Vulkan XML specification file (vk.xml)") {
		IsRequired = true, Arity = ArgumentArity.ExactlyOne
	};
	var libProjOption = new Option<FileInfo>("-l", "The path to the LLVK.csproj file") {
		IsRequired = true, Arity = ArgumentArity.ExactlyOne
	};
	var testProjOption = new Option<FileInfo>("-t", "The path to the LLVK.Tests.csproj file") {
		IsRequired = true, Arity = ArgumentArity.ExactlyOne
	};
	var specVersionOption = new Option<string>("-s", "The expected specification version (as X.X.X)") {
		IsRequired = true, Arity = ArgumentArity.ExactlyOne
	};
	var verboseOption = new Option<bool?>("-v", "Use verbose logging output") {
		Arity = ArgumentArity.Zero
	};

	// Build the command
	var cmd = new RootCommand("Generate the LLVK bindings");
	cmd.AddOption(specFileOption);
	cmd.AddOption(libProjOption);
	cmd.AddOption(testProjOption);
	cmd.AddOption(specVersionOption);
	cmd.AddOption(verboseOption);
	cmd.AddValidator(result => {
		var specFile = result.GetValueForOption(specFileOption);
		var outputProj = result.GetValueForOption(libProjOption);
		var testProj = result.GetValueForOption(testProjOption);
		var specVersion = result.GetValueForOption(specVersionOption);
		if (specFile is null || outputProj is null || testProj is null || specVersion is null) return;

		// Validate spec
		if (!Version.TryParse(specVersion, out _)) {
			result.ErrorMessage = $"The version '{specVersion}' is not of the form 'X.X.X'";
			return;
		}

		// Validate input
		if (!specFile.Exists) {
			result.ErrorMessage = $"The path '{specFile.FullName}' does not exist";
			return;
		}

		// Validate output
		if (!outputProj.Exists) {
			result.ErrorMessage = $"The path '{outputProj.FullName}' does not exist";
			return;
		}
		if (!testProj.Exists) {
			result.ErrorMessage = $"The path '{testProj.FullName}' does not exist";
		}
	});

	// Parse and return results
	var results = cmd.Parse(args);
	if (results is { Errors.Count: 0 }) {
		Log.SetVerbose(results.GetValueForOption(verboseOption) ?? false);
		return (
			results.GetValueForOption(specFileOption)!,
			Version.Parse(results.GetValueForOption(specVersionOption)!),
			results.GetValueForOption(libProjOption)!,
			results.GetValueForOption(testProjOption)!
		);
	}
	foreach (var error in results.Errors) {
		Log.Error($"Error: {error.Message}");
	}
	return null;
}
