# LLVK

Low-Level Vulkan Bindings (LLVK) is a set of auto-generated .NET 6+ bindings for Vulkan.

LLVK aims to be a very thin wrapper over the Vulkan C API, designed to minimize the overhead around preparing and using Vulkan types and calls. It also aims to add as little cruft as possible, making translating Vulkan C code to LLVK code easy (within the limitations of C#).

The majority of the library is code generated from the official Vulkan XML spec.

### Design

The following design features are central to the codebase:
- **No Marshaling** - Every API type is an unmanaged struct with a byte-perfect memory layout match to the corresponding C types.
- **Unmanaged Function Pointers** - All API functions are implemented as `delegate* unmanaged<...>`, no `DllImport` in sight.
- **Device Functions** - Device-level functions are loaded with `vkGetDeviceProcAddr` for the best performance.
- **Native Types** - Use of `nint` and `nuint` where applicable, and raw pointers instead of `IntPtr`.
- Extensive unit testing

There is also a small set of hand-crafted types that provide additional utilities for working with the API and unmanaged code in general.

### Using the Library

The two main library components are the `LLVK` namespace, which contains all of the hand-crafted support types, and the `LLVK.Vulkan` class which contains all of the generated Vulkan API code.

The intended use for the `Vulkan` class is to statically import it and its child classes to make Vulkan types and methods available in the current scope. The instance and device functions must also be loaded once the objects are created:

```csharp
// Include LLVK utilities
using LLVK;
// Imports core Vulkan API types/functions into the current scope
using static LLVK.Vulkan;
// Imports vendor specific types/functions into the current scope
using static LLVK.Vulkan.KHR; // + other vendors

... // Global functions only

vkCreateInstance(&createInfo, null, &instance)
VulkanLoader.LoadInstanceFunctions(instance);

... // Global + Instance level functions only

vkCreateDevice(physicalDevice, &createInfo, null, &device);
VulkanLoader.LoadDeviceFunctions(device);

... // All functions are available
```

A few usage notes:
- Naming:
  - Bitmask enums have `FlagBits` replaced with `Flags` (e.g. `VkImageUsageFlagBits` -> `VkImageUsageFlags`).
  - Enum values are normalized to PascalCase, have their common prefix removed, and have `_BIT` removed if present (e.g. `VK_IMAGE_USAGE_TRANSFER_SRC_BIT` -> `VkImageUsageFlags.TransferSrc`).
  - Structs, handles, and functions all have the same name as their spec types.
- Loading Instance and Device Functions:
  - After creating an instance or device, `LLVK.VulkanLoader.Load{Instance/Device}Functions(VkInstance/VkDevice)` must be called to load the functions.
  - `LLVK.VulkanLoader.IsFunctionLoaded(string)` can be used to check if an API function was successfully loaded.
- Utilities:
  - Extensions and layers can easily be queried with `InstanceExtensionSet`, `InstanceLayerSet`, and `DeviceExtensionSet`.
  - UTF-8 strings and lists of strings can be created and manipulated with `NativeStringUtf8` and `NativeStringListUtf8`.
  - `VulkanStructureChain<...>` can create and operate on linked chains of supported Vulkan structs.
  - All handle types have implicit boolean casts for nullity.
- All disabled/provisional/beta extensions and features are not generated.

There are also some important factors to keep in mind due to the pecularities of C#:
- It is important that the `SType` and `PNext` fields be initialized correctly on types that have them. This is done when calling the no-args constructor for those types, or by setting them manually.
- The library requires a 64-bit little-endian target.

#### Versions

The versioning of the LLVK library is identical to the version of the Vulkan specification that the code was generated against. A fourth version component indicates the code revision, making the final version `<sdk_major>.<sdk_minor>.<sdk_revision>.<revision>`. `<revision>` will only be bumped within a specific SDK version to fix critical issues or add features, and should always start at zero for each release.

### Building

The solution has two build configurations:
- `Generator|Any CPU` - Builds only the generator application.
- `Library|Any CPU` - Builds all projects. Still requires 64-bit at runtime, despite targeting "Any CPU".

#### Updating Bindings

The generator app is designed to panic if anything even slightly out of the ordinary is encountered which is not accounted for explicitly in the generator. This is to ensure that nothing that appears in new API versions is missed just because it is not explicitly handled. It is expected that each API update will require a few additions and changes to make the generator happy. It is documented in the code how to make these changes as needed.

If you are updating the library for a specific version of the headers, perform the following steps:
1. Update the Vulkan-Docs and Vulkan-Headers submodules to the desired version tag.
2. Update the versions in `LLVK.csproj` and `Generator.csproj` to match the SDK version.
3. Build the Generator project (with the `Generator|Any CPU` config), and run it with:
   - `-i <vk.xml>`, pointing to the XML spec file in Vulkan-Docs.
   - `-l <path/to/LLVK.csproj>`
   - `-t <path/to/LLVK.Tests.csproj>`
   - `-s <X.X.X>`, where `<X.X.X>` is the SDK version of the downloaded headers.
   - Optionally `-v` as well to help diagnose build issues.
4. Fix all of the errors that crop up during the header parsing stage, which may include (but are not limited to):
   - Supporting new platforms or beta specifications as they appear.
   - Adding new generated or manual types and tests.
   - Changing `SpecParser.cs` to handle new use cases.
   - Changing `SpecGenerator.cs` or any of the generation templates.
5. Run `build.{sh|bat}` in `LLVK.Tests/Native` to build the native testing code, and correct any build/generation issues.
6. Build the library (with the `Library|Any CPU` config), and run the unit tests and make changes until all are passing.

### Credits

- [Vulkan-Docs](https://github.com/KhronosGroup/Vulkan-Docs) ([MIT License](https://github.com/KhronosGroup/Vulkan-Docs/blob/main/LICENSE.adoc))
- [Vulkan-Headers](https://github.com/KhronosGroup/Vulkan-Headers) ([MIT License](https://github.com/KhronosGroup/Vulkan-Headers/blob/main/LICENSE.md))
- [Premake](https://premake.github.io/) ([BSD 3-Clause License](https://github.com/premake/premake-core/blob/master/LICENSE.txt))

### Legal

Both the LLVK generator application and library are licensed under the MIT license. See the LICENSE file at the root of this repository for more information.

Any third party tools are used under their original licenses, which can be found in the acknowledgement links above.
