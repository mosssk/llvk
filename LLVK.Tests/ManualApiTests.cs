﻿
using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.InteropServices;
using static LLVK.Vulkan;

namespace LLVK.Tests;


// Tests for the manually added Vulkan API features
public unsafe class ManualApiTests
{
	[Test]
	public void Test_VkBool32()
	{
		// Constants
		Assert.That(VkBool32.False, Is.EqualTo(false));
		Assert.That(VkBool32.True, Is.EqualTo(true));
		// Ctors
		Assert.That(new VkBool32(false), Is.EqualTo(false));
		Assert.That(new VkBool32(true), Is.EqualTo(true));
		// Casting
		Assert.That((VkBool32)false, Is.EqualTo(false));
		Assert.That((VkBool32)true, Is.EqualTo(true));
		Assert.True((bool)VkBool32.True);
		Assert.False((bool)VkBool32.False);
		// Equality
		Assert.That(VkBool32.True, Is.EqualTo(new VkBool32(true)));
		Assert.That(VkBool32.False, Is.EqualTo(new VkBool32(false)));
		Assert.That(VkBool32.True, Is.Not.EqualTo(new VkBool32(false)));
		Assert.True(VkBool32.True == new VkBool32(true));
		Assert.True(VkBool32.False == new VkBool32(false));
		Assert.True(VkBool32.True != new VkBool32(false));
		// Compare
		Assert.That(VkBool32.True.CompareTo(VkBool32.True), Is.EqualTo(0));
		Assert.That(VkBool32.False.CompareTo(VkBool32.False), Is.EqualTo(0));
		Assert.That(VkBool32.True.CompareTo(VkBool32.False), Is.Not.EqualTo(0));
		// Logical operators
		Assert.True (VkBool32.True  & VkBool32.True);
		Assert.False(VkBool32.True  & VkBool32.False);
		Assert.False(VkBool32.False & VkBool32.False);
		Assert.True (VkBool32.True  | VkBool32.True);
		Assert.True (VkBool32.True  | VkBool32.False);
		Assert.False(VkBool32.False | VkBool32.False);
		Assert.False(VkBool32.True  ^ VkBool32.True);
		Assert.True (VkBool32.True  ^ VkBool32.False);
		Assert.False(VkBool32.False ^ VkBool32.False);
	}

	[Test]
	[SuppressMessage("Assertion", "NUnit2007")]
	#pragma warning disable CS0618 // Type or member is obsolete
	public void Test_ApiVersionConstants()
	{
		Assert.That(VK_API_VERSION, Is.EqualTo(VK_MAKE_VERSION(1, 0, 0)));
		Assert.That(VK_API_VERSION_1_0, Is.EqualTo(VK_MAKE_API_VERSION(0, 1, 0, 0)));
		Assert.That(VK_API_VERSION_1_1, Is.EqualTo(VK_MAKE_API_VERSION(0, 1, 1, 0)));
		Assert.That(VK_API_VERSION_1_2, Is.EqualTo(VK_MAKE_API_VERSION(0, 1, 2, 0)));

		// This will detect a failure to add a test for new minor versions
		// TODO: Update this as new versions are introduced
		Assert.Null(typeof(Vulkan).GetField("VK_API_VERSION_1_3", BindingFlags.Public | BindingFlags.Static));
	}
	#pragma warning restore CS0618 // Type or member is obsolete
}
