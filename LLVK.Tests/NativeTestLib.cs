﻿/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace LLVK.Tests;


// Manages the loading/unloading of the native testing library and function loading
internal static class NativeTestLib
{
	// Library handle
	private static readonly IntPtr _NativeHandle;

	[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
	public static unsafe void* GetFunction(string name)
	{
		if (_NativeHandle == IntPtr.Zero) return null;
		return NativeLibrary.TryGetExport(_NativeHandle, name, out var addr) ? addr.ToPointer() : null;
	}

	static NativeTestLib()
	{
		// Try to load
		foreach (var path in enumerateNativePaths()) {
			if (NativeLibrary.TryLoad(path, out _NativeHandle)) break;
		}
		if (_NativeHandle == IntPtr.Zero) {
			throw new InvalidOperationException("Please build the LLVK native testing library first");
		}
		AppDomain.CurrentDomain.ProcessExit += (_, _) => NativeLibrary.Free(_NativeHandle);

		static IEnumerable<string> enumerateNativePaths()
		{
			var pre = OperatingSystem.IsWindows() ? "" : "lib";
			var ext = OperatingSystem.IsWindows() ? "dll" : "so";
			yield return Path.Combine(Environment.CurrentDirectory, $"Native/OUT/bin/{pre}llvk_native.{ext}");
			yield return Path.Combine(Environment.CurrentDirectory, $"../../../Native/OUT/bin/{pre}llvk_native.{ext}");
		}
	}
}
