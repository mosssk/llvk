## Testing Includes

This directory contains the code and artifacts required for the native parts of the LLVK tests.

This includes:
- Premake binaries for Windows/Linux
- Build scripts
- Spoofed platform headers
