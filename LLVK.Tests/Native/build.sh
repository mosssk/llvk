#!/bin/sh

### MIT License - Copyright (c) 2023 Sean Moss
### This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
### file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.

if [ "$(uname)" = "Darwin" ]; then
    chmod +x ./premake5_macos && ./premake5_macos --file=./llvk.project gmake2
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
    chmod +x ./premake5 && ./premake5 --file=./llvk.project gmake2
else
    echo "ERROR: Unknown build platform"
    exit 1
fi

(cd ./OUT && make -j4)
