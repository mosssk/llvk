
// This file spoofs the Xlib types required by the Vulkan headers

#pragma once

#include <cstdint>

struct Display;

typedef uint64_t VisualID;
typedef uint64_t Window;
