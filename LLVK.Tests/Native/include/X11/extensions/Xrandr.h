
// This file spoofs the Xlib/Xrandr types required by the Vulkan headers

#pragma once

#include <cstdint>

typedef uint64_t RROutput;
