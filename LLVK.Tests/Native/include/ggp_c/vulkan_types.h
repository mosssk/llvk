
// This file spoofs the GGP types required by the Vulkan headers

#pragma once

#include <cstdint>

typedef uint32_t GgpStreamDescriptor;
typedef uint32_t GgpFrameToken;
