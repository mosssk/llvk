
// This file spoofs the XCB types required by the Vulkan headers

#pragma once

#include <cstdint>

struct xcb_connection_t;

typedef uint32_t xcb_visualid_t;
typedef uint32_t xcb_window_t;
