
// This file spoofs the DirectFB types required by the Vulkan headers

#pragma once

struct IDirectFB;
struct IDirectFBSurface;
