
// This file spoofs the Fuchsia types required by the Vulkan headers

#pragma once

#include <cstdint>

typedef uint32_t zx_handle_t;
