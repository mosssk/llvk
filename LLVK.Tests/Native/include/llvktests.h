/*
 * MIT License - Copyright (c) 2023 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

#pragma once

#if defined(_MSC_VER)
#   define LLVK_EXPORT extern "C" __declspec(dllexport) 
#else
#   define LLVK_EXPORT extern "C" __attribute__((visibility("default"))) 
#endif

#include <cstdint>
#include <cstddef>

using U32 = uint32_t;
using I64 = int64_t;

/* TODO: Make sure to add more platform defines as they appear. */
#define VK_USE_PLATFORM_ANDROID_KHR
#define VK_USE_PLATFORM_FUCHSIA
#define VK_USE_PLATFORM_IOS_MVK
#define VK_USE_PLATFORM_MACOS_MVK
#define VK_USE_PLATFORM_METAL_EXT
#define VK_USE_PLATFORM_VI_NN
#define VK_USE_PLATFORM_WAYLAND_KHR
#define VK_USE_PLATFORM_WIN32_KHR
#define VK_USE_PLATFORM_XCB_KHR
#define VK_USE_PLATFORM_XLIB_KHR
#define VK_USE_PLATFORM_DIRECTFB_EXT
#define VK_USE_PLATFORM_XLIB_XRANDR_EXT
#define VK_USE_PLATFORM_GGP
#define VK_USE_PLATFORM_SCREEN_QNX

#include <vulkan/vulkan.h>
