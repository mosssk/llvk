
// This file spoofs the Windows types required by the Vulkan headers

#pragma once

#include <cstdint>

typedef void* HINSTANCE;
typedef void* HWND;
typedef void* HMONITOR;
typedef void* HANDLE;

struct SECURITY_ATTRIBUTES;

typedef uint32_t DWORD;
typedef const wchar_t* LPCWSTR;
