
// This file spoofs the wayland types required by the Vulkan headers

#pragma once

struct wl_display;
struct wl_surface;
