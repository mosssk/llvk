@echo off

@rem MIT License - Copyright (c) 2023 Sean Moss
@rem This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
@rem file at the root of this repository, or online at https://opensource.org/licenses/MIT.

start /D . /W /B premake5.exe --file=./llvk.project vs2022

for /F "delims=" %%i in ('.\vswhere.exe -property productPath -version 17.0') do set DEVENVCOM=%%i
if ["%DEVENVCOM%"] == [""] (
    echo "ERROR: Visual C++ Build Tools 17.0 (2022) not installed, please install before running"
    exit 1
)
set DEVENVCOM=%DEVENVCOM:.exe=.com%

echo Building project with: "%DEVENVCOM%"
"%DEVENVCOM%" ./OUT/LLVK.sln /Rebuild Build
