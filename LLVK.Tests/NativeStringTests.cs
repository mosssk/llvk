﻿
using System;
using System.Linq;
using System.Text;

namespace LLVK.Tests;


// Tests for the native string types
internal unsafe class NativeStringTests
{
	private const string
		STR1 = "string contents",
		STR2 = "string contents",
		STR3 = "string",
		STR4 = "special characters ™žτ\ud80c\udc03";

	private static readonly byte[] BYTES1 = { (byte)'a', (byte)'b', (byte)'c', (byte)'d' };


	[Test]
	public void Test_NativeString_Ctor()
	{
		Assert.That(new NativeStringUtf8().Length, Is.EqualTo(0));
		Assert.That(new NativeStringUtf8("").Length, Is.EqualTo(0));
		Assert.That(new NativeStringUtf8(STR1).Length, Is.EqualTo(Encoding.UTF8.GetByteCount(STR1)));
		Assert.That(new NativeStringUtf8(STR4).Length, Is.EqualTo(Encoding.UTF8.GetByteCount(STR4)));
		Assert.That(new NativeStringUtf8(BYTES1).Length, Is.EqualTo(BYTES1.Length));
	}

	[Test]
	public void Test_NativeString_Data()
	{
		using var str = new NativeStringUtf8(STR4);
		Assert.That(str.Data.SequenceCompareTo(Encoding.UTF8.GetBytes(STR4).AsSpan()), Is.EqualTo(0));
		using var str2 = new NativeStringUtf8(BYTES1);
		Assert.That(str2.Data.SequenceCompareTo(BYTES1.AsSpan()), Is.EqualTo(0));
	}

	[Test]
	public void Test_NativeString_IsNullTerminated()
	{
		using var str = new NativeStringUtf8(STR4);
		fixed (byte* strPtr = str.Data) {
			Assert.That(strPtr[str.Length], Is.EqualTo((byte)0));
		}
	}

	[Test]
	public void Test_NativeString_GetString()
	{
		Assert.That(new NativeStringUtf8().GetString(), Is.EqualTo(""));
		Assert.That(new NativeStringUtf8(STR1).GetString(), Is.EqualTo(STR1));
		Assert.That(new NativeStringUtf8(STR2).GetString(), Is.EqualTo(STR2));
		Assert.That(new NativeStringUtf8(STR3).GetString(), Is.EqualTo(STR3));
		Assert.That(new NativeStringUtf8(STR4).GetString(), Is.EqualTo(STR4));
		Assert.That(new NativeStringUtf8(BYTES1).GetString(), Is.EqualTo("abcd"));
	}

	[Test]
	public void Test_NativeString_Equality()
	{
		var str1 = new NativeStringUtf8(STR1);
		var str2 = new NativeStringUtf8(STR2);
		var str3 = new NativeStringUtf8(STR3);
		var str4 = new NativeStringUtf8(STR4);

		Assert.True(str1.Equals(str1));
		Assert.True(str1.Equals(STR1));
		Assert.True(str1 == STR1);
		Assert.True(str1.Equals(str2));
		Assert.True(str1 == str2);
		Assert.False(str1.Equals(str3));
		Assert.False(str1 == str3);
		Assert.False(str1.Equals(STR3));
		Assert.False(str1 == STR3);
		Assert.False(str1.Equals(str4));
		Assert.True(str1 != str4);
		Assert.False(str1.Equals(STR4));
		Assert.True(str1 != STR4);
	}

	[Test]
	public void Test_NativeString_Dispose()
	{
		NativeStringUtf8 str = new NativeStringUtf8();
		Assert.False(str.IsDisposed);
		str.Dispose();
		Assert.True(str.IsDisposed);
		Assert.Throws<ObjectDisposedException>(() => _ = str.Data);
		Assert.Throws<ObjectDisposedException>(() => _ = str.GetString());
	}


	[Test]
	public void Test_NativeStringList_Ctor()
	{
		Assert.That(new NativeStringListUtf8().Count, Is.EqualTo(0));
		Assert.That(new NativeStringListUtf8(8).Count, Is.EqualTo(0));
		Assert.That(new NativeStringListUtf8(new[] { "a", "b" }).Count, Is.EqualTo(2));
	}

	[Test]
	public void Test_NativeStringList_Data()
	{
		using var list = new NativeStringListUtf8(new[] { "ab", "cde" });
		Assert.That(list.Data[0][0], Is.EqualTo((byte)'a'));
		Assert.That(list.Data[0][1], Is.EqualTo((byte)'b'));
		Assert.That(list.Data[1][0], Is.EqualTo((byte)'c'));
		Assert.That(list.Data[1][1], Is.EqualTo((byte)'d'));
		Assert.That(list.Data[1][2], Is.EqualTo((byte)'e'));
	}

	[Test]
	public void Test_NativeStringList_StringsAreNullTerminated()
	{
		using var list = new NativeStringListUtf8(new[] { "ab", "cde" });
		Assert.That(list.Data[0][2], Is.EqualTo(0));
		Assert.That(list.Data[1][3], Is.EqualTo(0));
	}

	[Test]
	public void Test_NativeStringList_Indexer()
	{
		using var list = new NativeStringListUtf8(new[] { "ab", "cde" });
		var str1 = list[0];
		var str2 = list[1];

		Assert.Throws<IndexOutOfRangeException>(() => _ = list[2]);
		Assert.True(str1.SequenceEqual(new[] { (byte)'a', (byte)'b' }));
		Assert.True(str2.SequenceEqual(new[] { (byte)'c', (byte)'d', (byte)'e' }));
	}

	[Test]
	public void Test_NativeStringList_Dispose()
	{
		var list = new NativeStringListUtf8();
		Assert.False(list.IsDisposed);
		list.Dispose();
		Assert.True(list.IsDisposed);

		Assert.Throws<ObjectDisposedException>(() => _ = list[0]);
		Assert.Throws<ObjectDisposedException>(() => _ = list.GetString(0));
		Assert.Throws<ObjectDisposedException>(() => _ = list.GetEnumerator().MoveNext());
		Assert.Throws<ObjectDisposedException>(() => list.EnsureCapacity(1));
		Assert.Throws<ObjectDisposedException>(() => list.Add(""));
		Assert.Throws<ObjectDisposedException>(() => list.Add(new[] { 'a' }));
		Assert.Throws<ObjectDisposedException>(() => list.Add(new[] { (byte)0 }));
		Assert.Throws<ObjectDisposedException>(() => list.RemoveAt(0));
		Assert.Throws<ObjectDisposedException>(() => list.Clear());
	}

	[Test]
	public void Test_NativeStringList_GetString()
	{
		using var list = new NativeStringListUtf8(new[] { "ab", "cde" });
		Assert.That(list.GetString(0), Is.EqualTo("ab"));
		Assert.That(list.GetString(1), Is.EqualTo("cde"));
		Assert.Throws<ArgumentOutOfRangeException>(() => _ = list.GetString(2));
	}

	[Test]
	public void Test_NativeStringList_Enumeration()
	{
		using var list = new NativeStringListUtf8(new[] { "ab", "cde" });
		Assert.True(list.ToArray().SequenceEqual(new[] { "ab", "cde" }));
		Assert.True((new NativeStringListUtf8()).ToArray().SequenceEqual(Array.Empty<string>()));
	}

	[Test]
	public void Test_NativeStringList_Add()
	{
		using var list = new NativeStringListUtf8();
		Assert.That(list.Count, Is.EqualTo(0));

		list.Add("string1");
		Assert.That(list.Count, Is.EqualTo(1));
		Assert.That(list.GetString(0), Is.EqualTo("string1"));

		list.Add("string2".AsSpan());
		Assert.That(list.Count, Is.EqualTo(2));
		Assert.That(list.GetString(0), Is.EqualTo("string1"));
		Assert.That(list.GetString(1), Is.EqualTo("string2"));

		list.Add(Encoding.UTF8.GetBytes("bytes3"));
		Assert.That(list.Count, Is.EqualTo(3));
		Assert.That(list.GetString(0), Is.EqualTo("string1"));
		Assert.That(list.GetString(1), Is.EqualTo("string2"));
		Assert.That(list.GetString(2), Is.EqualTo("bytes3"));
	}

	[Test]
	public void Test_NativeStringList_RemoveAndClear()
	{
		using var list = new NativeStringListUtf8(new[] { "str1", "str2", "str3", "str4" });
		Assert.That(list.Count, Is.EqualTo(4));

		Assert.Throws<ArgumentOutOfRangeException>(() => list.RemoveAt(10));

		list.RemoveAt(^1);
		Assert.That(list.Count, Is.EqualTo(3));
		Assert.True(list.ToArray().SequenceEqual(new[] { "str1", "str2", "str3" }));

		list.RemoveAt(1);
		Assert.That(list.Count, Is.EqualTo(2));
		Assert.True(list.ToArray().SequenceEqual(new[] { "str1", "str3" }));

		list.Clear();
		Assert.That(list.Count, Is.EqualTo(0));
	}
}
